<?php

/**
Template Name: Le cf en bref
**/

get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="headband" style="background-image: url(<?php the_field('bandeau_image'); ?>);" >
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="content-headband">
							<?php 
							$picto = get_field('picto');
							if( !empty($picto) ): ?>
								<img src="<?php echo $picto['url']; ?>" alt="<?php echo $picto['alt']; ?>" />
							<?php endif; ?>
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="bck-page bck-top ">
					<div class="row">

						<div class="col-md-12">

							<!-- ///////////// FIL D'ARIANNE ////////////// -->

							<div class="fil-ariane">
								<a href="<?php echo site_url(); ?>">Accueil</a>
								<a href="">></a>
								<a class="active" href=""><?php the_title(); ?></a>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="page-content-enfi">
									<div class="col-md-8">

										<?php
										if( have_rows('contenu') ):
											while ( have_rows('contenu') ) : the_row(); ?>

												<?php if( get_row_layout() == 'titre_h2' ): ?>
		        								<div class="titre-enfi">
													<h2><?php the_sub_field('titre'); ?></h2>
												</div>

												<?php elseif( get_row_layout() == 'chapeau' ): ?>
				        							<div class="sous-titre-enfi">
				        								<?php the_sub_field('texte'); ?>
													</div>	

												<?php elseif( get_row_layout() == 'bloc' ): ?>
				        							<div class="section-page-enfi page-compte-perso">
														<h3><?php the_sub_field('titre'); ?></h3>

														<div class="txt-page-enfi">
															<?php the_sub_field('texte'); ?>
														</div>

														<div class="btn-cf">
															<a class="blue-btn" title="<?php the_sub_field('libelle_bouton'); ?>" href="<?php the_sub_field('url_bouton'); ?>"><?php the_sub_field('libelle_bouton'); ?></a>
														</div>
													</div>

												<?php endif;
											endwhile;
										endif; ?>
									</div>

									<div class="col-md-4">
										<?php
										if( have_rows('contenus') ):
											while ( have_rows('contenus') ) : the_row(); ?>

												<?php if( get_row_layout() == 'video' ): ?>
		        								<div class="img-enfi">
		        									<?php the_sub_field('script'); ?>
												</div>

												<?php elseif( get_row_layout() == 'chapeau' ): ?>
				        							<div class="sous-titre-enfi">
				        								<?php the_sub_field('texte'); ?>
													</div>	

												<?php elseif( get_row_layout() == 'chiffres_cles' ): ?>
				        							<div class="section-page-enfi page-cf">
														<h3><?php the_sub_field('titre'); ?></h3>
														<?php
														if( have_rows('chiffres') ):
															while ( have_rows('chiffres') ) : the_row(); ?>
																<div class="icon-cf">
																	<?php $image = get_sub_field('icone');
																	if( !empty($image) ): ?>
																		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																	<?php endif; ?>
																	<span><?php the_sub_field('chiffre'); ?></span>
																	<p><?php the_sub_field('texte'); ?></p>
																</div>
															<?php endwhile;
														endif; ?>
													</div>
												<?php endif;
											endwhile;
										endif; ?>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<?php

get_footer();