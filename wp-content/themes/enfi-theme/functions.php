<?php
/**
 * enfi-theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package enfi-theme
 */

if ( ! function_exists( 'enfi_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function enfi_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on enfi-theme, use a find and replace
	 * to change 'enfi-theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'enfi-theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'enfi-theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'enfi_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'enfi_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function enfi_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'enfi_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'enfi_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function enfi_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'enfi-theme' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'enfi_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function enfi_theme_scripts() {

	//////////////// BOOTSTRAP ///////////////

	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
	
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');




	//////////////// SLICK ////////////////

	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/slick/slick.css');
	
	wp_enqueue_style( 'slick-slider', get_template_directory_uri() . '/slick/slick-theme.css');

	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/slick/slick.js', array(), '20120206', true );

	// ///////////////////////////////////////

	wp_enqueue_style( 'enfi-theme-style', get_stylesheet_uri() );
	wp_enqueue_script( 'enfi-theme-taxo', get_template_directory_uri() . '/js/enfi.js', array(), '20151214', true);
	wp_localize_script('enfi-theme-taxo', 'ajaxUrl', admin_url('admin-ajax.php'));

	//////////////// BOOTSTRAP ////////////////


	wp_enqueue_script( 'landingfnh-js-bootsratp', get_template_directory_uri() . '/js/bootstrap.js', array(), '20120206', true );




	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'enfi_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

require get_template_directory() . '/inc/ajax.php';
require get_template_directory() . '/inc/gravity-forms-filters.php';


require_once('wp_bootstrap_navwalker.php');


register_taxonomy( 'your-taxonomy-name', 'your-post-type', array(
    'label' => __('Category'),

    // this parameter is used to enable
    // sorting for taxonomy 'your-taxonomy-name'
    'i_order_terms' => true,
));

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'THEMENAME' ),
	) 
);

register_post_type( 'formations',
    array(
      'labels' => array(
        'name' => __( 'Formations' ),
        'singular_name' => __( 'Formation' )
      ),
      'taxonomies' => array( 'post_tag' ),
      'public' => true,
    )
  );
register_post_type( 'partenaires',
    array(
      'labels' => array(
        'name' => __( 'Partenaires' ),
        'singular_name' => __( 'Partenaire' )
      ),
      'taxonomies' => array( 'post_tag' ),
      'public' => true,
    )
  );
register_taxonomy(   
	'categorie_formation',   
	array('formations'),   
	array(     
		'label' => 'Catégories de formation',     
		'labels' => array(     
			'name' => 'Catégories de formation',     
			'singular_name' => 'Catégorie de formation',      
			),   
		'hierarchical' => true   ) 
	);
register_taxonomy(   
	'type_formation',   
	array('formations'),   
	array(     
		'label' => 'Types de formation',     
		'labels' => array(     
			'name' => 'Types de formation',     
			'singular_name' => 'Type de formation',      
			),   
		'hierarchical' => true   ) 
	);

function enfi_filter_training_session( $value, $post_id, $field ) {	
	// bail early if no value
	if( empty($value) ) {
		return $value;
	}

	// vars
	$order = array();
	$post_count = 0;

	// populate order
	foreach( $value as $i => $row ) {
		if($post_count > 3) {
			unset($value[$i]);
		} else {
			if((int) $row['field_56a772416af04'] < (int) date('Ymd')) {
				unset($value[$i]);
				continue;
			} else {
				$order[ $i ] = $row['field_56a772416af04'];
				++$post_count;
			}
		}
	}
	
	// multisort
	array_multisort( $order, SORT_ASC, $value );
	
	// return	
	return $value;
}

add_filter('acf/load_value/key=field_56a772246af03', 'enfi_filter_training_session', 10, 3);
