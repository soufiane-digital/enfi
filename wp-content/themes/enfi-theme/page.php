<?php

/**
Template Name: Préparer sa venue
**/

get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="headband" style="background-image: url(<?php the_field('bandeau_image'); ?>);" >
			<div class="container">
				<div class="row">
					<div class="col-md-4 mobile-headband">
						<div class="content-headband">
							<?php 
							$picto = get_field('picto');
							if( !empty($picto) ): ?>
								<img src="<?php echo $picto['url']; ?>" alt="<?php echo $picto['alt']; ?>" />
							<?php endif; ?>
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="bck-page bck-top">

				<!-- ///////////////////////////////////////////////// -->

					<div class="row">
						<div class="col-md-12">

							<!-- ///////////// FIL D'ARIANNE ////////////// -->
							
							<div class="fil-ariane">
								<a href="">Accueil</a>
								<a href="">></a>
								<a class="active" href=""><?php the_title(); ?></a>
							</div>
							
							<!-- ///////////// CONTENUS ///////////// -->

							<?php
							if( have_rows('contenus') ):
								while ( have_rows('contenus') ) : the_row(); ?>

									<!-- //////////////////////////// -->

									<?php if( get_row_layout() == 'h2' ): ?>
	        					<div class="titre-enfi">
											<h2><?php the_sub_field('titre'); ?></h2>
										</div>

									<!-- //////////////////////////// -->

									

										<?php elseif( get_row_layout() == 'h3' ): ?>
        							<div class="section-page-enfi">
												<h3><?php the_sub_field('titre'); ?></h3>
											</div>

										<?php elseif( get_row_layout() == 'h4' ): ?>
        							<div class="section-page-enfi">
        								<ul>
        									<li>
														<h4><?php the_sub_field('titre'); ?></h4>
													</li>
												</ul>
											</div>

										<?php elseif( get_row_layout() == 'h5' ): ?>
		        					<div class="section-page-enfi container-h5">
												<h5><?php the_sub_field('titre'); ?></h5>
											</div>

										<?php elseif( get_row_layout() == 'chapeau' ): ?> 
											<div class="sous-titre-enfi">
												<?php the_sub_field('texte'); ?>
											</div>

										<?php elseif( get_row_layout() == 'paragraphe' ): ?> 
											<div class="section-page-enfi">
												<div class="txt-page-enfi">
													<?php the_sub_field('texte'); ?>
												</div>
											</div>

										<?php elseif( get_row_layout() == 'map' ): ?>
											<div class="section-page-enfi">
												<div class="row">
													<div class="col-md-6">
														<?php the_sub_field("contenu_map"); ?>
													</div>
													<div class="col-md-6">
														<div class="map-page-enfi">
															<?php if( have_rows('adresses') ): ?>
														    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
														    <div id="map" style="width: 100%; height: 300px;"></div>
																<script>
																	function initMap() {
				  													var mapCenter = {lat: 48.856614, lng: 2.352222};

																	  // Create a map object and specify the DOM element for display.
																	  var map = new google.maps.Map(document.getElementById('map'), {
																	    center: mapCenter,
																	    scrollwheel: false,
																	    zoom: 11
																	  });
																		<?php while ( have_rows('adresses') ) : the_row(); ?>
																			<?php $address = get_sub_field('adress'); ?>
																		  // Create a marker and set its position.
																		  var marker = new google.maps.Marker({
																		    map: map,
																		    position: {lat: <?php echo $address['lat']; ?>, lng: <?php echo $address['lng']; ?>},
																		    title: "<?php echo $address['address']; ?>"
																		  });
																		<?php endwhile; ?>
																	}
																</script>
															<?php endif; ?>
														</div>
													</div>
											</div> 

									

									<!-- /////////////////////////////////// -->
									<!-- //////////// ZONE DOC //////////// -->
									<!-- ///////////////////////////////// -->

									<?php elseif( get_row_layout() == 'zone_doc' ): ?> 
										<div class="section-page-enfi">
											<div class="page-presse">
												<?php
												if( have_rows('documents') ):
													while ( have_rows('documents') ) : the_row(); ?>
														<a href="<?php the_sub_field('fichier_pdf'); ?>" target="_blank" title="<?php the_sub_field('titre'); ?>">
															<div class="section-download-presse">
																<i class="fa fa-download"></i>
																<p><?php the_sub_field('titre'); ?></p>
															</div>
														</a>
													<?php endwhile;
												endif; ?>
											</div>
										</div>

									<!-- /////////////////////////////////// -->
									<!-- /////////// LISTE LOGOS /////////// -->
									<!-- /////////////////////////////////// -->

									<?php elseif( get_row_layout() == 'liste_logos' ): ?> 
									
									<div class="section-page-enfi page-partenaires">
										<?php
										if( have_rows('logos') ):
											while ( have_rows('logos') ) : the_row(); ?>
												<a href="<?php the_sub_field('site_partenaire'); ?>" target="_blank" title="Aller sur le site du partenaire">
													<?php 
													$logo = get_sub_field('logo');
													if( !empty($logo) ): ?>
														<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
													<?php endif; ?>
												</a>
											<?php endwhile;
										endif; ?>
										<div class="clearfix"></div>
									</div>

									<!-- /////////////////////////////////////// -->
									<!-- /////////// LISTE FORMATIONS ////////// -->
									<!-- /////////////////////////////////////// -->

									<?php elseif( get_row_layout() == 'liste-formations' ): ?>
										<div id="liste-formation-home">
											<div class="row">
												<div class="liste-formation-home">
													<ul>
														<?php
														if( have_rows('formations') ):
															while ( have_rows('formations') ) : the_row(); ?>
																<?php 
																$term = get_sub_field('formation');
																if( $term ): ?>
																	<li>
																		<a class="<?php echo get_field('class_css', $term); ?>" href="<?php the_sub_field('url'); ?>" title="<?php echo $term->name; ?>">
																			<?php 
																			$image = get_field('picto', $term);
																			if( !empty($image) ): ?>
																				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																			<?php endif; ?>
																			<h2><?php echo $term->name; ?></h2>
																		</a>
																	</li>
																<?php endif; ?>
															<?php 
															endwhile;
														endif; ?>
													</ul>
												</div>
											</div><!-- FIN ROW -->
										</div>

									<!-- ////////////////////////////////// -->
									<!-- /////////// ZONE LIEN //////////// -->
									<!-- ////////////////////////////////// -->

									<?php elseif( get_row_layout() == 'zone_lien' ): ?>
										<div class="btn-inscription">
											<h3><?php the_sub_field('texte_incitatif'); ?></h3>
											<a class="red-btn" href="<?php the_sub_field('lien'); ?>" title="<?php the_sub_field('libelle'); ?>"><?php the_sub_field('libelle'); ?></a>
										</div>
										<div class="clearfix"></div>

									<!-- /////////////////////////////////////////// -->
									<!-- //////////// PARAGRAPHE IMAGE //////////// -->
									<!-- ///////////////////////////////////////// -->

									<?php elseif( get_row_layout() == 'paragraphe_image' ): ?> 
										
										<div class="page-content-enfi">
											<div class="row">

												<!-- //////////////////////////// -->

												<div class="col-md-8">
													<?php
													if( have_rows('contenus_gauche') ):
														while ( have_rows('contenus_gauche') ) : the_row(); ?>

															<?php if( get_row_layout() == 'h2' ): ?>
																<div class="titre-enfi">
																	<h2><?php the_sub_field('titre'); ?></h2>
																</div>

																<!-- //////////////////////////// -->

																<?php elseif( get_row_layout() == 'h3' ): ?>
																	<div class="section-page-enfi">
																		<h3><?php the_sub_field('titre'); ?></h3>
																	</div>

																<!-- //////////////////////////// -->

																<?php elseif( get_row_layout() == 'h4' ): ?>
																	<div class="titre-enfi">
																		<h4><?php the_sub_field('titre'); ?></h4>
																	</div>

																<!-- //////////////////////////// -->

																<?php elseif( get_row_layout() == 'chapeau' ): ?>
																	<div class="sous-titre-enfi">
																		<?php the_sub_field('texte'); ?>
																	</div>

																<!-- //////////////////////////// -->

																<?php elseif( get_row_layout() == 'paragraphe' ): ?>
																	<div class="section-page-enfi">
																		<div class="txt-page-enfi">
																			<?php the_sub_field('texte'); ?>
																		</div>
																	</div>

																<!-- //////////////////////////// -->

															<?php endif; ?>
														<?php endwhile; 
													endif; ?>
												</div><!-- FIN COL-MD-8 -->

												<!-- //////////////////////////// -->

												<div class="col-md-4">
													<div class="img-enfi">
														<?php 
														$image = get_sub_field('image');
														if( !empty($image) ): ?>
															<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
														<?php endif; ?>
													</div>
												</div><!-- FIN COL-MD-4 -->

												<!-- //////////////////////////// -->

											</div><!-- FIN ROW -->
											<div class="clearfix"></div>
										</div><!-- FIN PAGE CONTENT ENFI -->


									<?php endif; ?>
								<?php endwhile;
							endif; ?>

							<!-- ///////////////////////////////////////////////// -->

						</div><!-- FIN COL-MD-12 -->
					</div><!-- FIN ROW -->
					
				<!-- ///////////////////////////////////////////////// -->

				</div><!-- FIN BCK-PAGE BCK-TOP -->
			</div><!-- FIN CONTAINER -->
		</div><!-- FIN BCK CONTENT -->
	</div><!-- FIN ROW -->
</div><!-- FIN CONTAINER-FLUID-->



<?php
get_footer();
