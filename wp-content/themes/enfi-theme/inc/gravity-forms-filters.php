<?php

if(!is_admin()) {
  add_filter( 'gform_field_container_4', 'enfi_field_container', 10, 6 );
  function enfi_field_container( $field_container, $field, $form, $css_class, $style, $field_content ) {
    if(strpos($css_class, 'start-training-block')) {
      return '<div class="formation-modale"><p>{FIELD_CONTENT}</p><div class="row">';
    } elseif(strpos($css_class, 'start-first-block')) {
      return '</div></div><div class="contact-modale"><div class="row"><div class="col-md-6"><p>{FIELD_CONTENT}</p>';
    } elseif(strpos($css_class, 'start-second-block')) {
      return '</div><div class="col-md-6"><div class="societe-modale"><p>{FIELD_CONTENT}</p><div class="col-2-form-formation">';
    } elseif(strpos($css_class, 'start-third-block')) {
      return '</div><div class="demande-modale"><p>{FIELD_CONTENT}</p>';   
    } elseif(strpos($css_class, 'end-block')) {
      return '</div></div></div></div><div class="row"><div class="infos-enfi-modale">{FIELD_CONTENT}</div></div>';
    } else {
      return '{FIELD_CONTENT}';
    }
  }

  add_filter( 'gform_field_container_4_1', 'enfi_field_container_1', 10, 6 );
  function enfi_field_container_1( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="info-form-1"><div class="col-md-10">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_4_2', 'enfi_field_container_2', 10, 6 );
  function enfi_field_container_2( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-2">{FIELD_CONTENT}</div><div class="clearfix"></div></div>';
  }

  add_filter( 'gform_field_container_4_3', 'enfi_field_container_3', 10, 6 );
  function enfi_field_container_3( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="info-form-2"><div class="col-md-4">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_4_4', 'enfi_field_container_4', 10, 6 );
  function enfi_field_container_4( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-4">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_4_39', 'enfi_field_container_39', 10, 6 );
  function enfi_field_container_39( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-4">{FIELD_CONTENT}</div><div class="clearfix"></div></div>';
  }

  add_filter( 'gform_field_container_4_30', 'enfi_field_container_30', 10, 6 );
  add_filter( 'gform_field_container_4_20', 'enfi_field_container_30', 10, 6 );
  function enfi_field_container_30( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="radio">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_get_form_filter_1', 'custom_schedule', 10, 2 );
  function custom_schedule( $form_string, $form ) {
    return $form_string;
  }

  add_filter('gform_pre_render_4', 'add_readonly_script');
  function add_readonly_script($form){ ?>
    <script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery("#input_4_1, #input_4_2").attr("readonly","readonly");
      });
    </script>
  <?php
    return $form;
  }



  
  add_filter( 'gform_field_container_7', 'enfi_field_container_7', 10, 6 );
  function enfi_field_container_7( $field_container, $field, $form, $css_class, $style, $field_content ) {
    if(strpos($css_class, 'start-training-block')) {
      return '<div class="formation-modale"><p>{FIELD_CONTENT}</p><div class="row">';
    } elseif(strpos($css_class, 'start-first-block')) {
      return '</div><div class="contact-modale"><div class="row"><div class="col-md-6"><p>{FIELD_CONTENT}</p>';
    } elseif(strpos($css_class, 'start-second-block')) {
      return '</div><div class="col-md-6"><div class="societe-modale"><p>{FIELD_CONTENT}</p><div class="col-2-form-formation">';
    } elseif(strpos($css_class, 'start-third-block')) {
      return '</div><div class="demande-modale"><p>{FIELD_CONTENT}</p>';   
    } elseif(strpos($css_class, 'end-block')) {
      return '</div></div></div></div><div class="row"><div class="infos-enfi-modale">{FIELD_CONTENT}</div></div>';
    } else {
      return '{FIELD_CONTENT}';
    }
  }

  add_filter( 'gform_field_container_7_1', 'enfi_field_container_7_1', 10, 6 );
  function enfi_field_container_7_1( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="info-form-1"><div class="col-md-10">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_7_2', 'enfi_field_container_7_2', 10, 6 );
  function enfi_field_container_7_2( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-2">{FIELD_CONTENT}</div><div class="clearfix"></div></div>';
  }

  add_filter( 'gform_field_container_7_3', 'enfi_field_container_7_3', 10, 6 );
  function enfi_field_container_7_3( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="info-form-2"><div class="col-md-4">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_7_4', 'enfi_field_container_7_4', 10, 6 );
  function enfi_field_container_7_4( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-4">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_7_5', 'enfi_field_container_7_5', 10, 6 );
  function enfi_field_container_7_5( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-4">{FIELD_CONTENT}</div><div class="clearfix"></div></div>';
  }

  add_filter( 'gform_field_container_7_30', 'enfi_field_container_7_30', 10, 6 );
  add_filter( 'gform_field_container_7_20', 'enfi_field_container_7_30', 10, 6 );
  function enfi_field_container_7_30( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="radio">{FIELD_CONTENT}</div>';
  }



  add_filter( 'gform_field_container_8', 'enfi_field_container_8', 10, 6 );
  function enfi_field_container_8( $field_container, $field, $form, $css_class, $style, $field_content ) {
    if(strpos($css_class, 'start-training-block')) {
      return '<div class="formation-modale"><p>{FIELD_CONTENT}</p><div class="row">';
    } elseif(strpos($css_class, 'start-first-block')) {
      return '</div></div><div class="contact-modale"><div class="row"><div class="col-md-6"><p>{FIELD_CONTENT}</p>';
    } elseif(strpos($css_class, 'start-second-block')) {
      return '</div><div class="col-md-6"><div class="societe-modale"><p>{FIELD_CONTENT}</p><div class="col-2-form-formation">';
    } elseif(strpos($css_class, 'start-third-block')) {
      return '</div><div class="demande-modale"><p>{FIELD_CONTENT}</p>';   
    } elseif(strpos($css_class, 'end-block')) {
      return '</div></div></div></div><div class="row"><div class="infos-enfi-modale">{FIELD_CONTENT}</div></div>';
    } else {
      return '{FIELD_CONTENT}';
    }
  }

  add_filter( 'gform_field_container_8_1', 'enfi_field_container_8_1', 10, 6 );
  function enfi_field_container_8_1( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="info-form-1"><div class="col-md-10">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_8_2', 'enfi_field_container_8_2', 10, 6 );
  function enfi_field_container_8_2( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-2">{FIELD_CONTENT}</div><div class="clearfix"></div></div>';
  }

  add_filter( 'gform_field_container_8_3', 'enfi_field_container_8_3', 10, 6 );
  function enfi_field_container_8_3( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="info-form-2"><div class="col-md-4">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_8_4', 'enfi_field_container_8_8', 10, 6 );
  function enfi_field_container_8_8( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-4">{FIELD_CONTENT}</div>';
  }

  add_filter( 'gform_field_container_8_5', 'enfi_field_container_8_5', 10, 6 );
  function enfi_field_container_8_5( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="col-md-4">{FIELD_CONTENT}</div><div class="clearfix"></div></div>';
  }

  add_filter( 'gform_field_container_8_30', 'enfi_field_container_8_30', 10, 6 );
  add_filter( 'gform_field_container_8_20', 'enfi_field_container_8_30', 10, 6 );
  function enfi_field_container_8_30( $field_container, $field, $form, $css_class, $style, $field_content ) {
    return '<div class="radio">{FIELD_CONTENT}</div>';
  }

  add_filter('gform_pre_render_8', 'add_readonly_script_8');
  function add_readonly_script_8($form){ ?>
    <script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery("#input_8_1, #input_8_2").attr("readonly","readonly");
      });
    </script>
  <?php
    return $form;
  }




}