<!-- ////////////// PARTAGER CETTE FORMATIONS ////////////// -->

<div class="partage-formations">
	<div class="row">

		
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="txt-partage-2 txt-partage-3"> 
				Partager cette actualité
			</div>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="partage-formation partage-actu">
				<a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&amp;hashtags=Formation, Crédit Foncier&amp;via=ENFI" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/formations/twitter.jpg" alt="Twitter"></a>

				<a href="https://www.linkedin.com/shareArticle?mini=true&url=http://enfi.com&title=<?php echo urlencode(get_the_title()); ?>&summary=Ecole Nationale du Financement de l'Immobilier&source=LinkedIn" target="_blank" title="Partager cette actualité"><img src="<?php echo get_template_directory_uri(); ?>/img/formations/linkedin.jpg" alt="Linkedin"></a>
				
				<a href="#" onclick="window.open('https://partners.viadeo.com/share?language=fr', '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=300, left=300, width=500, height=500'); return false;" class="vd-share-button"><img src="<?php echo get_template_directory_uri(); ?>/img/formations/viadeo.jpg" alt="Viadeo"></a>
			</div>
		</div>

	</div>
</div>