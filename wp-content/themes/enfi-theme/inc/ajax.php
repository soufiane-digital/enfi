<?php
add_action( 'wp_ajax_training_filters', 'training_filters_callback' );
add_action( 'wp_ajax_nopriv_training_filters', 'training_filters_callback' );

function training_filters_callback() {
	$datas = $_GET['data'];

  $taxos = array();
  $page = 1;

  foreach ($datas as $key => $value) {
    if(!empty($value['value'])) {
      if($value['name'] === 'page') {
        $page = (int)$value['value'];
      } else {
        array_push(
          $taxos, 
          array(
              'taxonomy'  =>  $value['name'],
              'field'     =>  'slug',
              'terms'   =>  $value['value'],
          )
        );
      }
    }
	}

  if(!empty($taxos)) {
    $taxos = array(
      'tax_query' => array(
        'relation' => 'AND',
        $taxos,
      )
    );
  }

  listing_training_posts($page, $taxos);
	wp_die();
}

function listing_training_posts($page, $more_args){
	global $post;
  
	$args = array( 'post_type' => 'formations', 'posts_per_page' => 10, 'orderby' => 'rand', 'paged' => $page);
	$merge = array_merge($args, $more_args);	
	$loop = new WP_Query( $merge );

	if(!$loop->have_posts()) {
    echo 'Aucun résultat pour ces filtres.';
  } else {
  	while ( $loop->have_posts() ) : 
  		$loop->the_post();
  		$taxonomy_categorie = 'categorie_formation';

  		$terms = get_the_terms( $post->ID , $taxonomy_categorie );
  		$taxonomy_type = 'type_formation';
  		$terms_type = get_the_terms( $post->ID , $taxonomy_type );

  	?>
  		<div class="col-md-6 col-sm-6">
  			<div class="bloc-catalogue"><?php
  				if ( !empty( $terms ) ):
  					foreach ( $terms as $term ) :?>
            <div>
  						<div class="picto" style="background-color:<?php the_field('couleur', 'categorie_formation_'.$term->term_id); ?>">
  						<?php 
  						$image = get_field('picto', $term);
  						if( !empty($image) ): ?>
  							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
  						<?php endif; ?>
  						</div>
  						<div class="txt-bloc-catalogue">
  							<h4>
  							<?php
  								$messagealimiter = get_the_title();
  								echo substr($messagealimiter, 0,205);
  								if(strlen ($messagealimiter) > 205){echo "...";}
  							?>
  							</h4>
  							<?php echo '<h5>' . $term->name . '</h5>'; ?>
  						</div>
              <div class="clearfix"></div>
            </div><?php 
  					endforeach;
  				endif; 
  				?>
  				<div class="content-bloc-catalogue">
  					<div class="categorie-catalogue">
              Type(s):  
  						<?php if ( !empty( $terms_type ) ) :
  							foreach ( $terms_type as $term_type ): ?>
                  <?php if($term_type->parent == 0) {continue;} ?>
  								<?php echo '<p class="category-terms">' . $term_type->name . '</p>'; ?><?php
  							endforeach;
  						endif;?>
  					</div>
  					<?php
  					if( have_rows('sessions') ): the_row(); ?>

              <div class="infos-formations-location">
    						<div class="row">
    							<div class="date-catalogue">
    								<div class="lieu-catalogue">
    									<i class="fa fa-map-marker"></i>
    									<p>Lieu</p>
    								</div>
    							</div>
    							<div class="date-catalogue">
    								<div class="lieu-catalogue">
    									<?php 
    									$lieu = get_sub_field('lieu');
    									if ( !empty($lieu) ): ?>
    									  <span><?php the_sub_field('lieu'); ?></span>
    								  <?php else: ?>
    									  <span>Nous consulter</span>
    								  <?php endif; ?>
    								</div>
    							</div>
    						</div>

    						<div class="row">
    							<div class="title-date">
    								<div class="date-catalogue">
    									<i class="fa fa-calendar"></i>
    									<p>DATE</p>
    								</div>
    							</div>
    							<div class="info-date">
    								<div class="date-catalogue">
    									<?php 
    									$date = get_sub_field('date');
    									if( !empty($date) ): ?>
    									<span><?php the_sub_field('date'); ?></span>
    									<?php else: ?>
    										<span>Nous consulter</span>
    									<?php endif; ?>
    								</div>
    							</div>
    						</div>
              </div>

            <?php else : ?>

              <div class="infos-formations-location">
                <div class="row">
                  <div class="date-catalogue">
                    <div class="lieu-catalogue">
                      <i class="fa fa-map-marker"></i>
                      <p>Lieu</p>
                    </div>
                  </div>
                  <div class="date-catalogue">
                    <div class="lieu-catalogue">
                      <span>Nous consulter</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="date-catalogue">
                    <div class="date-catalogue">
                      <i class="fa fa-calendar"></i>
                      <p>DATE</p>
                    </div>
                  </div>
                  <div class="date-catalogue">
                    <div class="date-catalogue">
                        <span>Nous consulter</span>
                    </div>
                  </div>
                </div>
              </div>
              
  					<?php endif; ?>
  					<div class="description-catalogue">
  						<?php 
  						$messagealimiter=get_field('extrait');
  						echo substr($messagealimiter, 0,140);
  						if(strlen ($messagealimiter) > 140){echo "...";}
  						?>	
  					</div>
  					<div class="btn-savoir-plus"><a href="<?php the_permalink(); ?>">en savoir +</a></div>
  				</div>
  			</div>
  		</div>  
  	<?php endwhile; 
      echo '<div class="clearfix"></div>';
      echo '<div id="pagination">';
      echo paginate_links(array(
      'current' => $page,
      'total'   => $loop->max_num_pages,
      'format'  => '?paged=%#%'
    ));
  	echo '</div>';
  }
	wp_reset_query(); 
}