<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package enfi-theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bck-page-actu">
				<div class="titre-actu-p">
					<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				</div>
			</div>
		</div>
	</header><!-- .entry-header -->
</article><!-- #post-## -->
