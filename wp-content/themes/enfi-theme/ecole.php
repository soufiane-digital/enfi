<?php

/**
Template Name: L'école
**/

get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="headband" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/ecole/ecole.jpg);" >

			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="content-headband">
							<img src="<?php echo get_template_directory_uri(); ?>/img/ecole/icon.png" alt="">
							<h1>L’école</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="bck-page bck-top ">
					<div class="row">

						<div class="col-md-12">
								<a href="">Accueil</a>
								<a href="">></a>
								<a class="active" href="">L'école</a>
							</div>
							
							<div class="titre-enfi">
								<h2>L’école nationale du financement de l’immobilier : <br/>des solutions pour la professionnalisation des métiers de l’immobilier et de son financement</h2>
							</div>
							<div class="clearfix"></div>
						</div>


						<div class="col-md-12">
							<div class="row">

								<div class="page-content-enfi">

									<div class="col-md-8">
										<div class="sous-titre-enfi">
											Créée en 2009, l’Ecole Nationale du Financement de l’Immobilier, filiale du Crédit Foncier (Groupe BPCE), est un centre de formation agréé dont la mission est d’accompagner tout professionnel en quête de nouveaux savoirs et savoir-faire liés à l’immobilier et à son financement. 
										</div>

										<div class="section-page-enfi">
											<h4>une école fondée sur L’expertise historique du crédit foncier</h4>

											<div class="txt-page-enfi">
												Capitalisant sur l’expertise historique de sa maison mère en matière de <span>financement de l’immobilier</span>, l’ENFI est un partenaire reconnu auprès des promoteurs, constructeurs de maisons individuelles, agents immobiliers, administrateurs de biens et courtiers ainsi que <span>des établissements issus de la Banque et de l’Assurance.</span>
											</div>

											<div class="txt-page-enfi">
												L’ENFI propose également des <span>formations à l’attention des décideurs</span> (Dirigeants, Directeurs administratifs et financiers, Directeurs immobiliers) en charge de la gestion et de l’optimisation d’actifs immobiliers d’entreprises du secteur privé, public ou médico-social.
											</div>

											<div class="txt-page-enfi">
												Aujourd’hui, les domaines d’expertise de l’ENFI couvrent un spectre large comme 
											</div>
										</div>

										<!-- /////////////// LISTE ECOLE /////////////// -->

										<div class="section-page-enfi liste-ecole">
											<ul>
												<li>
													<a href="">
														<img src="<?php echo get_template_directory_uri(); ?>/img/home/credit.jpg" alt="">
														<h4>le crédit immobilier pour les particuliers, </h4>
													</a>
												</li>

												<li>
													<a href="">
														<img src="<?php echo get_template_directory_uri(); ?>/img/home/financement.jpg" alt="">
														<h4>le financement des actifs immobiliers d’entreprises</h4>
													</a>
												</li>

												<li>
													<a href="">
														<img src="<?php echo get_template_directory_uri(); ?>/img/home/droit.jpg" alt="">
														<h4> l’expertise et la valorisation immobilière</h4>
													</a>
												</li>

												<li>
													<a href="">
														<img src="<?php echo get_template_directory_uri(); ?>/img/ecole/reglementation.jpg" alt="">
														<h4>réglementations liées à l’immobilier.</h4>
													</a>
												</li>
											</ul>
										</div>
										
									</div>

									<!-- /////////////// IMG ECOLE /////////////// -->

									<div class="col-md-4">
										<div class="img-enfi">
											<img src="<?php echo get_template_directory_uri(); ?>/img/ecole/img.jpg" alt="">	
										</div>
									</div>

									<!-- /////////////// UNE MARQUE /////////////// -->

									<div class="col-md-12">
										<div class="section-page-enfi">
											<h3>Une marque pédagogique singulière</h3>

											<div class="txt-page-enfi">
											Soucieuse de préserver une marque pédagogique qui lui est propre, l’ENFI veille au respect de la ligne de conduite qu’elle s’est fixée : adapter les programmes aux particularités de chaque entreprise et privilégier la dimension opérationnelle des sujets afin de permettre un rapide retour sur investissement pour l’entreprise et son collaborateur. A cette fin, ses intervenants ont été choisis parce qu’ils sont à la fois praticiens de la matière enseignée et pédagogues avérés; les collaborateurs et « jeunes retraités » du Crédit Foncier sont ainsi des ressources précieuses.
											</div>
										</div>

										<div class="section-page-enfi">
											<h4>des formules en distanciel adaptées</h4>
											<div class="txt-page-enfi">
												Depuis sa création, l’ENFI a mis en place des formules d’apprentissage à distance souples et individualisées, en termes de lieu, de temps et de rythme d’apprentissage. Utilisées seules ou associées à des temps de formation en présentiel, nos solutions associent interactivité et efficacité et peuvent prendre la forme de e.learning, e.book, classe virtuelle … avec une traçabilité et un accompagnement si besoin tout au long des apprentissages.
											</div>
										</div>

										<div class="section-page-enfi">
											<h4>Les parcours diplômants et les formations certifiantes</h4>
											<div class="txt-page-enfi">
												L’ENFI met à votre disposition son expertise en ingénierie de formation en vous proposant la construction de solutions spécifiquement adaptées à vos besoins. Par ailleurs, l’ENFI s’est dotée d’importants moyens pédagogiques adaptés à vos attentes et dispose d’espaces au centre de Paris et au cœur des régions.
											</div>
										</div>

										<div class="section-page-enfi">
											<h4>Les formules sur mesure</h4>
											<div class="txt-page-enfi">
												Des cursus conçus pour les professionnels qui cherchent à perfectionner leurs compétences tout en poursuivant leurs activités. Des parcours modulaires qui peuvent être suivis en tout ou partie et qui permettent d’obtenir reconnaissance et valorisation des compétences et de son expérience.
											</div>
										</div>

										<div class="section-page-enfi">
											<h4>Une réponse pédagogique de proximité</h4>
											<div class="txt-page-enfi">
												Pour accompagner l’ensemble des professionnels dans leur parcours, différentes formules sont proposées, en inter ou en intra, dans les locaux de l’entreprise ou dans les nôtres, sur tout le territoire.
											</div>
										</div>

										<div class="section-page-enfi">
											<h3>l’équipe</h3>
											<div class="sous-titre-enfi sous-titre-ecole">
											L’équipe de l’ENFI est structurée pour apporter des réponses ajustées aux demandes des professionnels de l’immobilier et de la banqu(e. Le pôle pédagogie développe tout au long de l’année des programmes aux contenus et aux modalités pédagogiques innovants, afin d’anticiper ou s’adapter aux évolutions de l’environnement économique, financier ou réglementaire. Le pôle commercial et administratif est mobilisé pour vous apporter les réponses et l’accompagnement nécessaires à la mise en place de vos programmes de formation.
											</div>
										</div>
									</div>

									<!-- /////////////// L'EQUIPE /////////////// -->

									<div class="row">
										<div class="col-md-7">
											<div class="ecole-equipe">
												<table>

													<!-- DIRECTION -->

													<tr>
												  	<th>Direction</th>
												  	<td>Jean Francois Metz</td>
												  	<td class="ecole-mail">jean-francois.metz@enfi.fr</td>
													</tr>
													
													<!-- PEDAGOQIE -->

													<tr>
													  <th>pédagogie</th>
													  <td>Isabelle Pineau</td>
													  <td class="ecole-mail">isabelle.pineau@enfi.fr</td>
													</tr>

													<tr>
													  <th class="ecole-th-none"></th>
													  <td>Toni Juet</td>
													  <td class="ecole-mail">toni.juet@creditfoncier.fr</td>
													</tr>

													<!-- COMMUNICATION -->

													<tr>
												   	<th>communication</th>
												  	<td>Chantal Harito </td>
												  	<td class="ecole-mail">chantal.harito@enfi.fr</td>
													</tr>

													<tr>
												   	<th class="ecole-th-none"></th>
												  	<td>Lindsay Moro</td>
												  	<td class="ecole-mail">lindsay.moro@enfi.fr</td>
													</tr>
													
													<!-- ADMINISTARTION -->

													<tr>
												   	<th>Administration</th>
												  	<td>Emmy Rakotobe</td>
												  	<td class="ecole-mail">emmy.rakotobe@creditfoncier.fr</td>
													</tr>

													<!-- COMPTABILITE -->

													<tr>
													  <th>comptabilité</th>
													  <td>Elisabeth Marques </td>
													  <td class="ecole-mail">elisabeth.dasilvamarques@creditfoncier.fr</td>
													</tr>

												</table>
											</div>
										</div>

										<div class="col-md-5">
											<div class="equipe-numero">
												<div class="img-enfi">
													<img src="<?php echo get_template_directory_uri(); ?>/img/ecole/equipe-ecole.jpg" alt="">
												</div>
												<div class="numero-tel">
													<i class="fa fa-phone"></i>
													<p>01 57 44 81 71</p>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							
						</div>

						<div class="col-md-12">

							<div class="btn-ecole">
								<h3>retrouvez tous les partenaires de l’enfi</h3>
								<a class="red-btn" href="#">les partenaires</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php

get_footer();