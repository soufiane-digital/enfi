
	jQuery(document).ready(function(){
	  jQuery('.carousel').slick({
	    
	  	infinite: true,
	  	slidesToShow: 1,
	  	dots: true,
	  	arrows: false,
	  	autoplay: true,
	  	autoplaySpeed: 3000,
	  	slidesToScroll: 1,

	  	responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        dots: false
		      }
		    }
	  	]
	  });
	});

	jQuery(document).ready(function(){
	  jQuery('.partenaires').slick({
	    
	  	infinite: true,
	  	slidesToShow: 3,
	  	dots: true,
	  	arrows: false,
	  	autoplay: false,
	  	autoplaySpeed: 4000,
	  	slidesToScroll: 1,

	  	responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: false,
		        centerPadding: '40px',
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: false,
		        centerPadding: '40px',
		        slidesToShow: 1
		      }
		    }
	  	]

	  });
	});

	jQuery(document).ready(function(){
	  jQuery('.actu').slick({
	    
	  	infinite: true,
	  	slidesToShow: 1,
	  	dots: true,
	  	arrows: false,
	  	autoplay: false,
	  	autoplaySpeed: 4000,
	  	slidesToScroll: 1
	  });
	});



jQuery(document).ready(function(){
	$('#formations-liste').click(function() {
		$('#hidden').val('');
		$('.bd-formation').removeClass("active");
		$( "input[type=checkbox]" ).prop('checked', false);
		update_trainings()
	});

	$('.link-taxo').click(function(e) {
		e.preventDefault();
		var taxo = jQuery(this).data('slug');
		var hidden = $('#hidden').val(taxo);
		$('form .hidden-page').val(null);
		$(".bd-formation").removeClass("active");
		$(this).find('.bd-formation').addClass("active");
		update_trainings();
		return false;
	});

	$("input[type=checkbox]").on("change", function() {
		$('form .hidden-page').val(null);
		update_trainings();
	});

	// Handle AJAX pagination
	$('.bloc-catalogue-formation').on('click', '#pagination .page-numbers', function(e) {
		e.preventDefault();
    var page = 1;
		var url = $(this).prop('href');
    var captured = /paged=([^&]+)/.exec(url);

    if(captured != null) {
    	page = captured[1];
    }
    
		$('form .hidden-page').val(page);
		update_trainings()
		return false;
	});

	$("#gform_4 #input_4_1, #gform_4 #input_4_2, #gform_4 #input_4_3, #gform_4 #input_4_4, #gform_4 #input_4_5").addClass('disabled').focus(function() {$('#gform_4 #input_4_7').focus()});

	$('.session-container .training-subscription').click(function(e) {
		e.preventDefault();
		var datasHolder = $(this).parents('.session-container').first();
		$('#gform_4 #input_4_1, #gform_4 #input_4_34').val(datasHolder.data('name'));
		$('#gform_4 #input_4_2, #gform_4 #input_4_33').val(datasHolder.data('code'));
		$('#gform_4 #input_4_3, #gform_4 #input_4_35').val(datasHolder.data('place'));
		$('#gform_4 #input_4_4, #gform_4 #input_4_37').val(datasHolder.data('date'));
		// $('#gform_4 #input_4_5').val(datasHolder.data('prices'));
		var json = datasHolder.data('prices');
		$('#input_4_39').find('option').remove();
		for (var i = json.length - 1; i >= 0; i--) {
			$('#input_4_39').append("<option value="+json[i]+">"+json[i]+"</option>");
		}
		$('#gform_4 #input_4_5').find('option').remove();
		$('#gform_4 #input_4_5').append('wesh');
		$('.bs-example-modal-lg').modal('show');
		return false;
	});

	$('.session-container .customized-quote').click(function(e) {
		e.preventDefault();
		var datasHolder = $(this).parents('.session-container').first();
		$('#gform_8 #input_8_1, #gform_8 #input_8_32').val(datasHolder.data('name'));
		$('#gform_8 #input_8_2, #gform_8 #input_8_36').val(datasHolder.data('code'));
		$('.modal.devis').modal('show');
		return false;
	});
});

jQuery(document).bind('gform_post_render', function(){
  console.log('gform_post_render');
});

function update_trainings(){
	$('.bloc-catalogue-formation').html('Chargement...');
	$.get(
		ajaxUrl,
		{
			'action': 'training_filters',
			'data': $('#form-filter').serializeArray()
		},
		function(response){
			$('.bloc-catalogue-formation').html(response);
			$("html, body").animate({ scrollTop: $('#scroll-to-formation').offset().top }, 1000);

		}
	);

}



