<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package enfi-theme
 */

get_header(); ?>


		<?php
		if ( have_posts() ) : ?>

			<div class="container-fluid">
				<div class="row">
					<div class="headband" style="background-image: url(<?php echo site_url(); ?>/wp-content/uploads/2016/04/acutalite-enfi-formation-immobilier-banque_1920x190_acf_cropped.jpg);" >

						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<div class="content-headband page-list">
										<img src="<?php echo get_template_directory_uri(); ?>/img/catalogue/icon-catalogue.png" alt="">
										<h1>Résultats de la recherche</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid">
				<div class="row">
					<div class="bck-content">
						<div class="container">
							<div class="bck-page bck-top ">
								<div class="row">
									<div class="col-md-12">
										<h2>Les pages qui sont suceptibles de vous intéresser ...</h2>
											<?php
											/* Start the Loop */
											while ( have_posts() ) : the_post();

												/**
												 * Run the loop for the search to output the results.
												 * If you want to overload this in a child theme then include a file
												 * called content-search.php and that will be used instead.
												 */
												get_template_part( 'template-parts/content', 'search' );

											endwhile;
										endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php
get_footer();
