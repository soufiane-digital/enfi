<?php

/**
Template Name: Page d'accueil
**/

get_header(); ?>

	<!-- ////////////////////// CARROUSEL ////////////////////// -->

	<div class="carousel">
		<?php
		if( have_rows('slides') ):
			while ( have_rows('slides') ) : the_row(); ?>
				<div class="slide" style="background-image: url('<?php the_sub_field('photo'); ?>')">
					<div class="container">
						<div class="row">
							<div class="col-md-5 mobile-headband">
								<div class="content-slide-container">
									<div class="content-slide">
										<div class="title-carousel"><?php the_sub_field('titre'); ?></div>
										<div class="title-carousel-2"><?php the_sub_field('titre_2'); ?></div>
										<?php the_sub_field('texte'); ?>
										<a class="red-btn" href="<?php the_sub_field('url'); ?>" title="<?php the_sub_field('libelle'); ?>"><?php the_sub_field('libelle'); ?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile;
		endif; ?>
	</div>

	<!-- ////////////////////// FORMATION ////////////////////// -->

	<div id="liste-formation-home">
		<div class="container">
			<div class="row">
				<h1><?php the_field('titre'); ?></h1>
				<div class="liste-formation-home">
					<ul>
						<?php
						if( have_rows('formations') ):
							while ( have_rows('formations') ) : the_row(); ?>
								<?php 
								$term = get_sub_field('formation');
								if( $term ): ?>
									<li>
										<a class="<?php echo get_field('class_css', $term); ?>" href="<?php the_sub_field('url'); ?>" title="<?php echo $term->name; ?>">
											<?php 
											$image = get_field('picto', $term);
											if( !empty($image) ): ?>
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											<?php endif; ?>
											<h2><?php echo $term->name; ?></h2>
										</a>
									</li>
								<?php endif; ?>
							<?php 
							endwhile;
						endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- ////////////////////// CONTENT HOME PAGE ////////////////////// -->

	<div class="container-fluid">
		<div class="row">
			<div class="bck-content">
				<div class="container">
					<div class="row">

						<!-- ********* FORMATIONS ENFI *********-->

						<div class="col-md-8">
							<div class="bck-page">
								<div class="row">

									<div class="col-md-12">
										<div class="formation-enfi">
											<img src="<?php the_field('visuel'); ?>" alt="">
											<div class="txt-img-formation">
												<?php the_field('titre_type_formation'); ?>
											</div>
										</div>
									</div>

									<!-- ********* FORMATIONS DISTANCE *********-->

									<div class="col-md-6">
										<div class="formation-enfi-list">
											<h3><?php the_field('titre_gauche'); ?></h3>
										</div>
										<div class="picto-list">
											<?php
											if( have_rows('types_formations_gauche') ):
												while ( have_rows('types_formations_gauche') ) : the_row(); ?>
													<?php 
													$type = get_sub_field('type_de_formation');
													if( $type ): ?>
														<div class="picto-liste-enfi">
															<a class="link-formation <?php echo get_field('class_css', $type); ?>" href="<?php the_sub_field('url'); ?>" title="<?php echo $type_2->name; ?>">
																<div class="link-formation-content">
																	<?php 
																	$image_type = get_field('picto', $type);
																	if( !empty($image_type) ): ?>
																		<img src="<?php echo $image_type['url']; ?>" alt="<?php echo $image_type['alt']; ?>" />
																	<?php endif; ?>
																	<h4><?php echo $type->name; ?></h4>
																	<span class="icon-enfi-plus-2"></span>
																</div>
																<div class="hover-picto-formation"></div>
															</a>
														</div>
													<?php endif; ?>
												<?php 
												endwhile;
											endif; ?>
										</div>
									</div>

									<!-- ********* FORMATIONS PRESENTIELLES *********-->

									<div class="col-md-6">
										<div class="formation-enfi-list">
											<h3><?php the_field('titre_droite'); ?></h3>
										</div>

										<div class="picto-list">
											<?php
											if( have_rows('types_formations_droite') ):
												while ( have_rows('types_formations_droite') ) : the_row(); ?>
													<?php 
													$type_2 = get_sub_field('type_de_formation');
													if( $type_2 ): ?>
														<div class="picto-liste-enfi">
															<a class="link-formation <?php echo get_field('class_css', $type_2); ?>" href="<?php the_sub_field('url'); ?>" title="<?php echo $type_2->name; ?>">
																<div class="link-formation-content">
																	<?php 
																	$image_type = get_field('picto', $type_2);
																	if( !empty($image_type) ): ?>
																		<img src="<?php echo $image_type['url']; ?>" alt="<?php echo $image_type['alt']; ?>" />
																	<?php endif; ?>
																	<h4><?php echo $type_2->name; ?></h4>
																	<span class="icon-enfi-plus-2"></span>
																</div>
																<div class="hover-picto-formation"></div>
															</a>
														</div>
													<?php endif; ?>
												<?php 
												endwhile;
											endif; ?>
										</div>
									</div>

								</div>
							</div>

							<!-- ********* CARROUSEL PARTENAIRES *********-->

							<div class="bck-page bck-bottom">
								<div class="row">
									<div class="col-md-12">
										<div class="carrousel-partenaires">
											<h3><?php the_field('titre_3'); ?></h3>
											<div class="partenaires">
												<?php
												$the_query_2 = new WP_Query(array( 'post_type' => 'partenaires', 'posts_per_page' => 20 ));
												while ( $the_query_2->have_posts() ) : $the_query_2->the_post(); ?>
												  	<?php 
													$logo = get_field('logo');
													if( !empty($logo) ): ?>
														<a href="<?php the_field('lien'); ?>" title="<?php the_title(); ?>" target="blank">
															<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
														</a>
													<?php endif;
												endwhile; ?>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>

						</div>

						<!-- ////////////////////// SIDEBAR ////////////////////// -->

						<!-- ********* ACTUALITES *********-->
						
						<div class="col-md-4">
							<div class="row">

								<div id="actualites">

									<div class="col-md-12">
										<div class="titre-actualites">
											<i class="fa fa-newspaper-o"></i>
											<h3>Actualités</h3>
										</div>
									</div>

									<div class="col-md-12">
										<div class="bck-page bck-top">
											<div class="actu">
												<?php
												$the_query = new WP_Query( 'posts_per_page=3' ); 
												while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
													<div class="video-right hover-mask">
														<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
															<?php 
															$image = get_field('image_une');
															if( !empty($image) ): ?>
																<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
															<?php endif; ?>
															<span class="glyphicon glyphicon-search"></span>
															<div class="titre-actu-p"><?php the_title(); ?></div>
															<div class="date">
																<i class="fa fa-calendar-o"></i>
																<p><?php the_time('j/m/Y'); ?></p>
															</div>
														</a>
													</div>
												<?php endwhile; wp_reset_query(); ?>
											</div>
										</div>
									</div>

									<div class="clearfix"></div>
									
								</div>

								<!-- ********* NEWSLETTER *********-->

								<div id="newsletter">
									<div class="col-md-12">
										<div class="bck-page">
											<div class="newsletter-home">
												<h3><?php the_field('titre_newsletter'); ?></h3>
												<div class="side-bar-home">
													<p><?php the_field('texte_newsletter'); ?></p>
													<?php 
													gravity_form(1, false, false, false, '', false );
													?>
												</div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>

								<!-- ********* CONTACT *********-->

								<div id="contact">
									<div class="col-md-12">
										<div class="bck-page bck-bottom">
											<div class="contact-home">
												<div class="side-bar-home">
													<a class="red-btn" href="" data-toggle="modal" data-target=".bs-contact-modal-lg">CONTACTEZ  L’ENFI</a>
												</div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


<?php
get_footer();
