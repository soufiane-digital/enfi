<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package enfi-theme
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,300' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri(); ?>/css/menu.css" rel='stylesheet' type='text/css'>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="https://file.myfontastic.com/7P6LRmJYmrSwuPURFguprQ/icons.css" rel="stylesheet">

	<meta property="og:title" content="<?php the_title(); ?>" />
	<meta property="og:description" content="Description of shared article" />
	<meta property="og:url" content="http://example.com/my_article.html" />
	<meta property="og:image" content="http://enfi.dev.digital-expression.fr/wp-content/uploads/2016/04/formation-immobilier-banque-enfi_523x225_acf_cropped.jpg" />
</head>



<body <?php body_class(); ?>>
	<script type="text/javascript" src="http://creditfoncier.com/wp-content/themes/credit-foncier/assets/js/toolbar.js?show_social=false&theme=1&deep_fixing=true&lang=fr"></script>
	<header id="masthead" class="site-header">

		<nav class="navbar navbar-default navbar-fixed-top">
		  
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="bg-main-menu">
		    	<div class="container">
				    <div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu-enfi" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
				    	<a class="navbar-brand" href="<?php echo home_url(); ?>">
						    <img src="<?php echo get_template_directory_uri(); ?>/img/header/logo-enfi-2.png" alt="">
						</a>
				    </div>

				    <div class="navbar-right">
				    	<div class="search-bar">
				    		<a href="" data-toggle="modal" data-target="#myModalSearch"><i class="fa fa-search"></i></a>
				    	</div>
				    </div>

				    

				    <?php
	            	wp_nav_menu( array(
		                'menu'              => 'menu-principal',
		                'theme_location'    => 'menu-principal',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse navbar-right',
		        		'container_id'      => 'main-menu-enfi',
		                'menu_class'        => 'nav navbar-nav navbar-right',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
		        	?>

	        	</div>
			</div>

			<div class="bg-contact-header">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="contact-header">
					      	<div class="number">
			            	<span class="glyphicon glyphicon-earphone"></span>
			            	<a href="tel:0151448171" title="0151448171">01 57 44 81 71</a>
			            </div>

			            <div class="brand-bpce">
			            	<img src="<?php echo get_template_directory_uri(); ?>/img/header/logo-bpce.png" alt="">
			            </div>
					      </div>
				      </div>
				    </div>
			    </div>
			</div>

		</nav>		
	</header>

	<div id="content" class="site-content">
