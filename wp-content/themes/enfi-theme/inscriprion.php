<?php

/**
Template Name: Comment s'inscrire
**/

get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="headband" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/inscription/bck-inscription.jpg);" >

			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="content-headband">
							<img src="<?php echo get_template_directory_uri(); ?>/img/inscription/icon-inscription.png" alt="">
							<h1>Comment s’inscrire </h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="bck-page bck-top ">
					<div class="row">



						<div class="col-md-12">

							<!-- ///////////// FIL D'ARIANNE ////////////// -->

							<div class="fil-ariane">
								<a href="">Accueil</a>
								<a href="">></a>
								<a class="active" href="">Comment s’inscrire</a>
							</div>
							
							<!-- ///////////// TTRE ACTU ENFI ///////////// -->

							<div class="titre-enfi">
								<h2>Comment s’inscrire à l’enfi</h2>
							</div>
							<div class="clearfix"></div>
						</div>


						<div class="col-md-12">
							<div class="row">

								<div class="page-content-enfi">

									<div class="col-md-8">

										<div class="sous-titre-enfi">
											Vous êtes un professionnel et vous avez repéré une formation ?
										</div>

										<div class="section-page-enfi">
											<div class="txt-page-enfi">
												Vous pouvez vous inscrire via les formulaires présents sur chaque programme de formation, en choisissant entre la formule inter-entreprise ou intra entreprise.
											</div>

											<div class="txt-page-enfi">
												Pour toute autre demande nous sommes à même de vous proposer un programme sur-mesure répondant à vos besoins de formation particuliers, <a href="">contactez-nous :</a>
											</div>
										</div>

										<div class="section-page-enfi">
											<div class="row">
												<div class="col-md-5">
													<div class="cercle-inscription first-cercle"><i class="fa fa-phone"></i></div>
													<div class="numero-inscription">01 57 44 81 71</div>
												</div>

												<div class="col-md-7">
													<div class="cercle-inscription"><i class="fa fa-envelope"></i></div>
													<div class="numero-inscription">bal@enfi.fr</div>
												</div>
											</div>
										</div>

										<div class="section-page-enfi">
											<span>Attention :</span> Si vous souhaitez vous faire refinancer votre formation professionnelle par un organisme agréé, veuillez compter un délais d’un mois en moyenne pour les délais administratifs.
										</div>
									</div>

									<div class="col-md-4">
										<div class="img-enfi">
											<img src="<?php echo get_template_directory_uri(); ?>/img/inscription/img-inscription.jpg" alt="">
										</div>
									</div>

									<div class="clearfix"></div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="btn-inscription">
								<h3>bien préparer sa venue à l’enfi </h3>
								<a class="red-btn" href="">préparer sa venue</a>
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<?php

get_footer();