<?php

/**
Template Name: formations
**/

get_header(); ?>

<?php
  $taxonomy_categorie = 'categorie_formation';
  $terms = get_the_terms( $post->ID , $taxonomy_categorie );
  $term = $terms[0];

  $training_name = strip_tags(get_the_title());
  $training_code = strip_tags(get_field('id_formation'));
?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
  <div class="row">
    <div class="headband" style="background-image: url(<?php the_field('bandeau_image', $term); ?>);" >
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="content-headband headband-formation" style="background-color:<?php the_field('couleur', $term); ?>">
              <?php 
              $image = get_field('picto', $term);
              if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              <?php endif; ?>
              <?php echo '<h2>' . $term->name . '</h2>' ?>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- ///////////////////////////////////////////////// -->
<!-- ///////////////// FORMATIONS //////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
  <div class="row">
    <div class="bck-content">
      <div class="container">
        <div class="bck-page bck-top ">

          <!-- ////////////// PRET A TAUX ZERO ////////////// -->

          <div class="pret-formation">
            <div class="title">
              <h1><?php the_title(); ?></h1>
            </div>
            <div class="row">
              <div class="col-md-9">
                <div class="title">
                  <?php 
                  $chapeau = get_field('chapeau');
                  if( !empty($chapeau) ): ?>
                    <?php the_field('chapeau'); ?>
                  <?php endif; ?>
                </div>
              </div>

              <div class="col-md-3">
                <div class="btn-contact">
                  <a href="" data-toggle="modal" data-target=".bs-contact-modal-lg">
                    <div class="content-contact">
                      <i class="fa fa-envelope"></i>
                      <p>CONTACTEZ-NOUS</p>
                    </div>
                  </a>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>

          <div class="bloc-infos-formation">
            <div class="row">

              <!-- ////////////// PAROLE D'EXPERT ////////////// -->

              <div class="parole-expert">
                <div class="col-md-7">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="titre-rubrique formation"><?php the_field('titre_de_la_zone'); ?></div>
                    </div>
                  </div>

                  <?php 
                  if(get_field('zone') == "expert") : ?>
                    <div class="content-formations">
                      <div class="row">
                        <div class="col-md-3 col-sm-3">
                          <?php 
                          $image = get_field('photo_expert');
                          if( !empty($image) ): ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                          <?php endif; ?>
                        </div>

                        <div class="col-md-9 col-sm-9">
                          <?php the_field('parole_dexpert'); ?>
                          <div class="who">
                            <p class="name"><?php the_field('nom_expert'); ?></p>
                            <span>-</span>
                            <p class="job"><?php the_field('poste_expert'); ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                  <?php 
                  if(get_field('zone') == "image") : ?>
                    <div class="content-formations">
                      <div class="row">
                        <div class="col-md-12">
                          <a data-toggle="modal" data-target=".parcours-modal" title="Découvrir le document" target="_blank">
                            <?php 
                            $image = get_field('image');
                            if( !empty($image) ): ?>
                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <?php endif; ?>
                          </a>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
              </div>

              <!-- Modal 1 Activé-->
            
            <?php 
              $image = get_field('pdf');
              if( !empty($image) ): ?>
              <div id="modal-parcours" class="modal fade parcours-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">

										<div class="modal-header" style="border-top: 12px solid <?php the_field('couleur', $term); ?>">
                      <div class="modal-icon-round" style="background-color: <?php the_field('couleur', $term); ?>">
                        <?php 
                            $image = get_field('picto', $term);
                            if( !empty($image) ): ?>
                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <?php endif; ?>
                      </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">LE programme</h4>
                    </div>

                    <div class="modal-body">
                      <div class="image">
                        <img src="<?php the_field('pdf'); ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endif; ?>

              <!-- ////////////// PROCHAINE SESSION ////////////// -->

              <div class="prochaine-session">
                <div class="col-md-5">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="titre-rubrique formation">Prochaine session</div>
                    </div>
                  </div>
                  <?php
                  $training_sessions = get_field('sessions');
                  if( have_rows('sessions') ): the_row(); ?>
                    <?php 
                      $training_date = strip_tags(get_sub_field('date'));
                      $training_place = strip_tags(get_sub_field('lieu'));
                      $training_price = strip_tags(get_field('tarif'));
                    ?>

                        <div class="content-formations session-container" data-date="<?php echo $training_date; ?>" data-place="<?php echo $training_place; ?>" data-name="<?php echo $training_name; ?>" data-code="<?php echo $training_code; ?>" data-price="<?php echo $training_price; ?>">
                          <div class="row">
                            <div class="col-md-6 col-sm-6">
                              <div class="info-formation">
                                <?php 
                                $lieu = get_sub_field('lieu');
                                if( !empty($lieu) ): ?>
                                  <div class="lieu-formation">
                                    <i class="fa fa-map-marker"></i>
                                    <p>Lieu</p>
                                    <span><?php the_sub_field('lieu'); ?></span>
                                  </div>
                                <?php endif; ?>
                                <?php 
                                $date = get_sub_field('date');
                                if( !empty($date) ): ?>
                                  <div class="date-formation">
                                    <i class="fa fa-calendar"></i>
                                    <p>Date</p>
                                    <span><?php the_sub_field('date'); ?></span>
                                  </div>
                                <?php endif; ?>
                                <?php
                                if( have_rows('tarif') ): 
                                  while ( have_rows('tarif') ) : the_row(); ?>
                                    <div class="tarif-formation">
                                      <i class="fa fa-eur"></i>
                                      <p><?php the_sub_field('type_tarif'); ?></p>
                                      <span><?php the_sub_field('prix'); ?></span>
                                    </div>
                                  <?php endwhile;
                                endif; ?>
                                <?php 
                                $duree = get_sub_field('duree');
                                if( !empty($duree) ): ?>
                                  <div class="date-formation">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p>Durée</p>
                                    <span><?php the_sub_field('duree'); ?></span>
                                  </div>
                                <?php endif; ?>
                              </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                              <?php 
                              $fiche_recap = get_sub_field('fiche_recap');
                              if( !empty($fiche_recap) ): ?>
                                <div class="btn-file">
                                  <a href="<?php the_sub_field('fiche_recap'); ?>" targer="_blank" title="Fiche recap <?php the_title(); ?>">
                                    <div class="content-file">
                                      <i class="fa fa-file-o"></i>
                                      <p>fiche recap</p>
                                    </div>  
                                  </a>
                                </div>
                              <?php endif; ?>

                              <!-- ///// INSCRIPTION ///// -->

                              <?php
                              if( get_sub_field('formulaire_dinscription') ) : ?>
                                <div class="btn-file">
                                  <button type="button" class="content-file training-subscription" data-toggle="modal" data-target=".bs-example-modal-lg">
                                    <span class="icon-pen"></span>
                                    inscription
                                  </button>
                                </div>

																<!-- Modal 1 Activé-->

																<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
																  <div class="modal-dialog modal-lg">
																    <div class="modal-content">


																	    <div class="modal-header" style="border-top: 12px solid <?php the_field('couleur', $term); ?>">
																	      <div class="modal-icon-round" style="background-color: <?php the_field('couleur', $term); ?>">
																	        <?php 
																	            $image = get_field('picto', $term);
																	            if( !empty($image) ): ?>
																	              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																	            <?php endif; ?>
																	      </div>   
																	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
																				<h4 class="modal-title" id="myModalLabel"><?php echo '<h2>' . $term->name . '</h2>' ?></h4>
                                        <h5>PRÉ-Inscription </h5>																	      
																	    </div>   

																      <div class="modal-body">
																        <?php 
																          gravity_form(4, false, false, false, array(), true, '', true );
																        ?>
																      </div>


																    </div>
																  </div>
																</div>  
                              <?php endif; ?>

                              <!-- ///// DEVIS ///// -->

                              <?php
                              if( get_sub_field('devis_sur-mesure') ) : ?>
                                <div class="btn-file">
                                  <a href="" data-toggle="modal" data-target=".bs-contact-modal-lg">
                                    <div class="content-file">
                                      <i class="fa fa-pencil-square-o"></i>
                                      <p>devis sur mesure</p>
                                    </div>  
                                  </a>
                                </div>
                              <?php endif; ?> 


                            </div>  
                          </div>
                        </div>
                  <?php else: ?>
                    <div class="no-session">Aucune session prévue pour le moment.</div>
                  <?php endif; ?>
                  <?php wp_reset_query(); ?>
                </div>
              </div>
            </div>
          </div>

          <!-- ///////////////////////// TABS ///////////////////////// -->

          <div class="tabs-formation">

            <ul class="nav nav-tabs" role="tablist">
              <li class="col-md-4 col-sm-4 col-xs-12 active" role="presentation">
                <a href="#programme" aria-controls="programme" role="tab" data-toggle="tab">
                  <i class="fa fa-leanpub"></i>
                  Programme
                </a>
                <div class="triangle-tabs"></div>
              </li>

              <li class="col-md-4 col-sm-4 col-xs-12" role="presentation">
                <a href="#infos" aria-controls="infos" role="tab" data-toggle="tab">
                  <i class="fa fa-info-circle"></i>
                  INFOS PRATIQUES
                </a>
                <div class="triangle-tabs"></div>
              </li>

              <li class="col-md-4 col-sm-4 col-xs-12" role="presentation">
                <a href="#dates" aria-controls="dates" role="tab" data-toggle="tab">
                  <i class="fa fa-calendar"></i>
                  Dates de sessions
                </a>
                <div class="triangle-tabs"></div>
              </li>
            </ul>

            <!-- ///////////////////////// PROGRAMME ///////////////////////// -->

            <div class="tab-content">

              <div role="tabpanel" class="tab-pane active" id="programme">

                <div class="programme-list">

                  <?php 
                  $partenaire = get_field('partenaire');
                  if( !empty($partenaire) ): ?>
                    <h4><?php the_field('partenaire'); ?></h4>
                  <?php endif; ?>

                  <!-- ////////////// COLONNE DE GAUCHE ////////////// -->
                  <div class="col-md-6">
                    <?php
                    if( have_rows('contenus_gauche') ):
                      while ( have_rows('contenus_gauche') ) : the_row(); ?>

                        <!-- ////////////// TITRE H4 ////////////// -->
                        <?php
                        if( get_row_layout() == 'sous_titre_h4' ): ?>
                          <div class="row">
                            <h4><?php the_sub_field('h4'); ?></h4>
                          </div>

                        <?php elseif( get_row_layout() == 'chapitre_programme' ): ?> 
                          <div class="row">
                            <h3><?php the_sub_field('titre'); ?></h3>
                            <?php the_sub_field('contenu_chapitre'); ?>
                          </div>

                        <?php elseif( get_row_layout() == 'image_fullwidth' ): ?> 
                          <div class="row">
                            <?php 
                            $image = get_sub_field('image');
                            if( !empty($image) ): ?>
                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" style="width:100%;" />
                            <?php endif; ?>
                          </div>

                        <?php elseif( get_row_layout() == 'paragraphe_texte' ): ?> 
                          <div class="row">
                            <?php the_sub_field('texte'); ?>
                          </div>

                        <?php endif; ?>

                      <?php endwhile;
                    endif; ?>
                  </div>

                  <div class="col-md-6">
                    <?php
                    if( have_rows('contenus_droite') ):
                      while ( have_rows('contenus_droite') ) : the_row(); ?>

                        <!-- ////////////// TITRE H4 ////////////// -->

                        <?php
                        if( get_row_layout() == 'sous_titre_h4' ): ?>
                          <div class="row">
                            <h4><?php the_sub_field('h4'); ?></h4>
                          </div>

                        <?php elseif( get_row_layout() == 'chapitre_programme' ): ?> 
                          <div class="row">
                            <h3><?php the_sub_field('titre'); ?></h3>
                            <?php the_sub_field('contenu_chapitre'); ?>
                          </div>

                        <?php elseif( get_row_layout() == 'image_fullwidth' ): ?> 
                          <div class="row">
                            <?php 
                            $image = get_sub_field('image');
                            if( !empty($image) ): ?>
                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" style="width:100%;" />
                            <?php endif; ?>
                          </div>

                        <?php elseif( get_row_layout() == 'paragraphe_texte' ): ?> 
                          <div class="row">
                            <?php the_sub_field('texte'); ?>
                          </div>

                        <?php endif; ?>

                      <?php endwhile;
                    endif; ?>
                  </div>
                </div>

                <!-- ////////////// OBJECTIF PEDAGOGIQUE ////////////// -->

                <?php
                if( have_rows('objectifs') ):
                  while ( have_rows('objectifs') ) : the_row(); ?>
                
                    <div class="pedagogique">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="row no-gutters bg-grey">

                            <div class="col-md-3">
                              <div class="obj-ped">
                                <i class="fa fa-line-chart"></i>
                                <h6>Objectifs<br>Pédagogiques</h6>
                                <div class="tri-obj"></div>
                              </div>
                            </div>

                            <div class="col-md-9">
                              <div class="content-objectifs">
                                <ul>
                                  <?php
                                  if( have_rows('liste-objectifs') ):
                                    while ( have_rows('liste-objectifs') ) : the_row(); ?>
                                      <li><?php the_sub_field('objectif'); ?></li>
                                    <?php endwhile;
                                  endif; ?>
                                </ul>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                
                  <?php endwhile;
                endif; ?>

                <!-- ////////////// METHODES PEDAGOGIQUE ////////////// -->

                <?php
                if( have_rows('methode') ):
                  while ( have_rows('methode') ) : the_row(); ?>

                    <div class="pedagogique">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="row no-gutters bg-grey">

                            <div class="col-md-3">
                              <div class="obj-ped meth-ped">
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                <h6>MÉTHODES<br>PÉDAGOGIQUES</h6>
                                <div class="tri-obj tri-meth"></div>
                              </div>
                            </div>

                            <div class="col-md-9">
                              <div class="content-objectifs">
                                <ul>
                                  <?php
                                  if( have_rows('liste-methodes') ):
                                    while ( have_rows('liste-methodes') ) : the_row(); ?>
                                      <li><?php the_sub_field('methode'); ?></li>
                                    <?php endwhile;
                                  endif; ?>
                                </ul>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>

                  <?php endwhile;
                endif; ?>

              </div>

              <!-- ///////////////////////// INFOS ///////////////////////// -->

              <div role="tabpanel" class="tab-pane" id="infos">

                <div class="row">
                  <div class="col-md-3">
                    <div class="infos-form">
                      <i class="fa fa-check-circle"></i>
                      <h2>Prérequis</h2>
                      <?php the_field('prerequis'); ?> 
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="infos-form">
                      <i class="fa fa-user"></i>
                      <h2>POUR QUI ?</h2>
                      <?php the_field('cibles'); ?> 
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="infos-form">
                      <i class="fa fa-star"></i>
                      <h2>évaluation</h2>
                      <?php the_field('evaluation'); ?> 
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="infos-form">
                      <i class="fa fa-eur"></i>
                      <h2>Tarifs</h2>
                      <?php the_field('tarif'); ?>
                    </div>
                  </div>
                </div>


              </div>

              <!-- ///////////////////////// DATES ///////////////////////// -->

              <div role="tabpanel" class="tab-pane" id="dates">
                
                  <?php
                  if( have_rows('sessions') ):
                    while ( have_rows('sessions') ) : the_row(); ?>
                      <?php 
                        $training_date = strip_tags(get_sub_field('date'));
                        $training_place = strip_tags(get_sub_field('lieu'));
                        $training_price = strip_tags(get_field('tarif'));
                      ?>
                      <div class="col-md-4 session-container" data-date="<?php echo $training_date; ?>" data-place="<?php echo $training_place; ?>" data-name="<?php echo $training_name; ?>" data-code="<?php echo $training_code; ?>" data-price="<?php echo $training_price; ?>">
                        <div class="container-date">
                          <div class="date-form">
                            <i class="fa fa-calendar"></i>
                            <p><?php the_sub_field('date'); ?></p>
                          </div>

                          <div class="date-form">
                            <i class="fa fa-map-marker"></i>
                            <p><?php the_sub_field('lieu'); ?></p>
                          </div>

                          <div class="btn-file btn-date-p">
                            <a href="javascript:void(0);" class="training-subscription">
                              <div class="content-file btn-date">
                                <span class="icon-pen"></span>
                                inscription
                              </div>  
                            </a>
                          </div>
                        </div>
                      </div>
                    <?php endwhile;
                  else : ?>
                    <?php if($training_sessions == false): ?>
                      Aucune session prévue pour le moment.
                    <?php else : ?>
                      Aucune autre session prévue pour le moment.
                    <?php endif; ?>
                  <?php endif; ?>
                
              </div>
              <!-- ////////////// PARTAGER CETTE FORMATIONS ////////////// -->
              <?php include('inc/partager-formation.php') ?>
            </div>
          </div>
          <div class="clearfix"></div>
        </div><!-- page blanche -->
        
        <!-- //////////////////// SUGESTIONS DE FORMATIONS //////////////////// -->
        <?php 
        $posts = get_field('formations_suggeres'); 
        if( $posts ): ?>
          <div class="sug-form">
            <div class="row">
              <div class="titre-rubrique"><?php the_field('titre_rubrique'); ?></div>
              <div class="row">
                <?php foreach( $posts as $post):;// variable must be called $post (IMPORTANT) ?>
                  <?php 
                    setup_postdata($post); 
                    $terms = get_the_terms( $post->ID , $taxonomy_categorie );
                    $term = $terms[0];
                  ?>
                  <div class="col-md-4">
                    <div class="bloc-catalogue">
                      <div>
                        <div class="picto" style="background-color:<?php the_field('couleur', $term); ?>">
                          <?php 
                          $image = get_field('picto', $term);
                          if( !empty($image) ): ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                          <?php endif; ?>
                        </div>
                        <div class="txt-bloc-catalogue">
                          <h4>
                            <?php 
                            $messagealimiter = get_the_title();
                            echo substr($messagealimiter, 0,255);
                            if(strlen ($messagealimiter) > 255){echo "...";}
                            ?>  
                          </h4>
                          <?php echo '<h5>' . $term->name . '</h5>'; ?>
                        </div>
                        <div class="clearfix"></div>
                      </div>                      
                      <div class="content-bloc-catalogue">
                        <?php
                        if( have_rows('sessions') ): the_row(); ?>
                          <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4">
                              <div class="lieu-catalogue">
                                <i class="fa fa-map-marker"></i>
                                <p>Lieu</p>
                              </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8">
                              <div class="lieu-catalogue">
                                <?php 
                                $lieu = get_sub_field('lieu');
                                if( !empty($lieu) ): ?>
                                  <span><?php the_sub_field('lieu'); ?></span>
                                <?php else: ?>
                                  <span>Nous consulter</span>
                                <?php endif; ?>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-4">
                              <div class="date-catalogue">
                                <i class="fa fa-calendar"></i>
                                <p>DATE</p>
                              </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8">
                              <div class="date-catalogue">
                                <?php 
                                $date = get_sub_field('date');
                                if( !empty($date) ): ?>
                                  <span><?php the_sub_field('date'); ?></span>
                                <?php else: ?>
                                  <span>Nous consulter</span>
                                <?php endif; ?>
                              </div>
                            </div>
                          </div>
                        <?php endif; ?>
                        <div class="formation-desc">
                          <?php 
                          $messagealimiter = get_field('extrait');
                          echo substr($messagealimiter, 0,140);
                          if(strlen($messagealimiter) > 140) {echo "...";}
                          ?>        
                        </div>                  
                        <div class="btn-savoir-plus">
                          <a href="<?php the_permalink(); ?>" title="Voir <?php the_title(); ?>">en savoir +</a>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?> 
            </div>
          </div>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>





<?php

get_footer();