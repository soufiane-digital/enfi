<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package enfi-theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">

		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<ul class="footer-menu">
							<li><a href="/mentions-legales/" title="mentions légales">Mentions Légales</a></li>
							<li>|</li>
							<li>© ENFI</li>
							<li>|</li>
							<li>Ecole Nationale du Financement de l'Immobilier 4 quai de Bercy 94224 Charenton Cedex 01 57 44 81 71</li>
						</ul>

						<div class="footer-social">
							Suivez nous :
							<a href="https://www.linkedin.com/company/enfi-ecole-nationale-du-financement-de-l'immobilier-" target="_blank" title="linked-in enfi"><i class="icon-linkedin"></i></a>
							<a href="http://fr.viadeo.com/fr/profile/ecole-nationale-du-financement-de-l-immobilier.enfi" target="_blank" title="viadeo enfi"><i class="icon-viadeo"></i></a>
							<a href="https://twitter.com/enfi_formation?lang=fr" target="_blank" title="twitter enfi"><i class="icon-twitter"></i></a>
							<a href="https://www.facebook.com/pages/Ecole-Nationale-du-Financement-De-Limmobilier/522518614465942?fref=ts" target="_blank" title="facebook enfi"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</div>

					</div>
				</div>
			</div>
		</div>

	</footer>

<!-- Modal Contact-->

<div class="modal fade bs-contact-modal-lg" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<div class="modal-icon-round">
					<img src="<?php echo get_template_directory_uri ();?>/img/icon-contact.png" alt="contact">
				</div>
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			    </button>
			    <h4 class="modal-title">Nous contacter</h4>
			</div>

			<div class="modal-body">
				<?php 
					gravity_form(7, false, false, false, array(), true, '', true );
				?>
			</div>
		</div>
  </div>
</div>

<!-- Modal recherche-->

<div class="modal fade" id="myModalSearch" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">Vous cherchez ....</h4>
      		</div>
     		<div class="modale-footer">
     			<?php get_search_form(); ?>
      		</div>
		</div>
	</div>
</div>

<!-- Modal mentions légales-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" >Mentions légales</h4>
      		</div>
     		<div class="modale-footer">

        		
        <p>
	        <span>Dénomination sociale : </span>ECOLE NATIONALE DU FINANCEMENT DE L’IMMOBILIER (ENFI)<br>
					<span>SAS au capital de : </span>410.206 € 504 381 153 RCS Paris. Enregistrée auprès de la Préfecture de la Région Ile-de -France sous le numéro <span>11 75 44708 75.</span><br>
					<span>Identifiant C.E. : </span>FR Siège social : 19, rue des Capucines - 75 001 Paris.<br>
					<span>Représentant légal : </span>Bruno Deletre, Président<br>
					<span>Directeur de la publication : </span>Jean-François Metz<br>
					<span>Contact : </span>Chantal Harito<br>
					<span>Hébergement : </span>Concepteur : Digital Expression<br>
					<span>Référencement : </span>E-volutions<br>
					<span>Crédit photos : </span>Arnaud Février, Fotolia, Jupiter Images, Phovoir, Shutterstock<br><br>
					L'éditeur s'engage à respecter l'ensemble des lois concernant la mise en place et l'activité d'un site Internet.<br>
				</p>
				
				<div class="mention-footer">ENFI attire l’attention des usagers de son site sur les points précisés ci-après :</div>
				
				<div class="section-page-enfi section-mention">
					<p>1 - ACCÈS AU SITE</p>
					L'utilisateur de ce site reconnaît disposer de la compétence et des moyens nécessaires pour accéder et utiliser ce site. Il est rappelé que le fait d'accéder ou de se maintenir frauduleusement dans un système informatique, d'entraver ou de fausser le fonctionnement d'un tel système, d'introduire ou de modifier frauduleusement des données dans un système informatique constituent des délits passibles de sanctions pénales. L'accès au site enfi.fr est gratuit. Les frais d'accès et d'utilisation du réseau de télécommunication sont à la charge du client, selon les modalités fixées par ses fournisseurs d'accès et opérateurs de télécommunication.
				</div>


				<div class="section-page-enfi section-mention">
					<p>2 - PROPRIÉTÉ INTELLECTUELLE</p>
					ENFI est propriétaire, ou titulaire des droits, de tous les éléments qui composent ce site notamment les données, dessins, graphiques, photos, bandes sonores. Toute reproduction, représentation, diffusion ou rediffusion, totale ou partielle, du contenu de ce site par quelque procédé que ce soit sans l'autorisation expresse et préalable d’ENFI est interdite, et constituerait une contrefaçon sanctionnée par les articles L 335 - 2 et suivants du Code de la Propriété Intellectuelle. Les marques de l'éditeur du site ENFI et de ses partenaires, ainsi que les logos figurant sur le site sont des marques déposées. Toute reproduction totale ou partielle de ces marques ou de ces logos, effectuée à partir des éléments du site sans l'autorisation expresse d’ENFI ou de son ayant- droit est donc prohibée, au sens de l'article L 713 - 2 du Code la Propriété Intellectuelle.
				</div>
						
				<div class="section-page-enfi section-mention">
					<p>3 - RESPONSABILITÉ</p>
					ENFI  se réserve le droit de corriger, à tout moment et sans préavis, le contenu de ce site. En outre, ENFI décline toute responsabilité en cas de retard, d'erreur ou d'omission quant au contenu des présentes pages de même qu'en cas d'interruption ou de non - disponibilité du service. ENFI ne peut être tenu pour responsable de toute décision prise sur la base d'une information contenue sur le site, ni de l'utilisation qui pourrait en être faite par des tiers. Toute personne souhaitant bénéficier d'un ou de plusieurs des services et/ou d'un ou de plusieurs produits présentés sur le site est invitée à contacter ENFI  pour s'informer des conditions contractuelles et tarifaires applicables à ce (ou ces) produit(s) et/ou ce (ou ces) service(s). L'accès aux produits et services présentés sur le site peut faire l'objet de restrictions à l'égard de certaines personnes. Aucun des produits et/ou services ne sera fourni par ENFI à une personne si la loi l'interdit. Cependant il appartient à toute personne intéressée de vérifier préalablement auprès de ses conseils habituels que son statut juridique et fiscal lui permet de souscrire des produits et/ou des services présentés sur le site.
				</div>

				<div class="section-page-enfi section-mention">
					<p>4 - DOMMAGES TECHNIQUES</p>
					ENFI ne saurait être tenu responsable des éléments en dehors de son contrôle et des dommages qui pourraient éventuellement être subis par l'environnement technique de l'utilisateur et notamment, ses ordinateurs, logiciels, équipements réseaux et tout autre matériel utilisé pour accéder ou utiliser le service et/ou les informations?
				</div>

				<div class="section-page-enfi section-mention">
					<p>5 - LIENS HYPERTEXTES</p>
					Le site enfi.fr contient des liens hypertextes permettant l'accès à des sites qui ne sont pas édités par ENFI. En conséquence ce dernier ne saurait être tenu pour responsable du contenu des sites auxquels l'internaute aurait ainsi accès. La création de liens hypertextes vers le site enfi.fr doit faire l'objet d'une autorisation préalable de l'éditeur.
				</div>

				<div class="section-page-enfi section-mention">
					<p>6 - COOKIES</p>
					L'utilisateur est informé que lors de ses visites sur le site, un cookie peut s'installer automatiquement et être conservé temporairement en mémoire ou sur son disque dur. Un cookie est un élément qui ne permet pas d'identifier l'utilisateur mais sert à enregistrer des informations relatives à la navigation de celui-ci sur le site internet. Les utilisateurs du site reconnaissent avoir été informés de cette pratique et autorisent  ENFI à l'employer. Ils pourront désactiver ce cookie par l'intermédiaire des paramètres figurant au sein de leur logiciel de navigation.
				</div>

				<div class="section-page-enfi section-mention">
					<p>UTILISATION DES COOKIES</p>
					Cette rubrique vous permet d'en savoir plus sur l'origine et l'usage des informations de navigation traitées à l'occasion de votre consultation de notre site www.enfi.fr Lors de votre consultation du site, des informations relatives à votre navigation sont susceptibles d'être enregistrées dans des fichiers appelés "Cookies" ou « Traceurs » installés sur votre terminal (ordinateur, tablette, Smartphone, etc.). Ces cookies sont émis par notre site ou par des partenaires tiers dans le but de faciliter votre navigation sur notre site. Ces cookies ont pour but de collecter et de stocker des informations relatives à votre navigation sur le site afin de vous adresser des services personnalisés.
				</div>
				
				<div class="section-page-enfi section-mention">
					<p>QU'EST-CE QU'UN COOKIE ?</p>
					Un cookie est un petit fichier contenant diverses informations textuelles. Il est déposé sur votre terminal (ordinateur, tablette, smartphone...) via votre navigateur, par le site web que vous visitez.
				</div>

				<div class="section-page-enfi section-mention">
					<p>LES DIFFÉRENTS TYPES DE COOKIES</p>
					Notre site utilise des cookies pour faciliter votre navigation et collecter des données statistiques.

					Vous trouverez ci-dessous la liste et fonctionnalité de nos cookies :

					• Le cookie _pk_ses.7.cb38 est le cookie de session du PHP

					• Les cookies _pk_id.7.cb38 et _pk_ref.7.cb38 sont les cookies du logiciel Piwik

					• Le cookie cookie-agreed-fr permet de ne plus afficher le message concernant les cookies lors d’une visite ultérieure pour un utilisateur.

					• Le cookie has_js est un cookie utilisé pour la gestion des Javascript
				</div>
				 

				<div class="section-page-enfi section-mention">
					COMMENT ACCEPTER OU REFUSER L'UTILISATION DE COOKIES ?
					Vous pouvez accepter l'utilisation des cookies ou choisir à tout moment de les désactiver. Le paramétrage se fait généralement depuis votre navigateur. Celui-ci peut être paramétré pour vous signaler les cookies qui sont déposés sur votre terminal et vous demander de les accepter ou non. Vous pouvez accepter ou refuser les cookies au cas par cas selon l'émetteur ou bien les refuser systématiquement une fois pour toutes. Afin de gérer les cookies en fonction de vos souhaits, il convient de paramétrer votre navigateur en tenant compte de la finalité des cookies telle que mentionnée ci-avant. Pour empêcher ou contrôler l’enregistrement des cookies, la configuration, qui varie selon chaque navigateur, est dans le menu d’aide de votre navigateur. Pour désactiver les cookies :

					Si vous utilisez le navigateur Internet Explorer Dans votre navigateur, cliquez sur Outils (en haut à droite) > Options Internet > Général > Paramètres > Afficher les fichiers. Supprimez la ligne commençant par « cookie » et contenant l’adresse de notre site internet.
					Si vous utilisez le navigateur Mozilla Firefox Dans votre navigateur, cliquez sur le menu (en haut à droite) > Options > Vie privée > Supprimer des cookies spécifiques. Supprimez la ligne contenant l’adresse de notre site internet.
					Si vous utilisez le navigateur Safari : Dans votre navigateur, cliquez sur le menu (en haut à droite) > Préférences > Confidentialité > Détails Sélectionnez le cookie contenant l’adresse de notre site internet et cliquez sur « Supprimer ».
					Si vous utilisez le navigateur Google Chrome Dans votre navigateur, cliquez sur le menu (en haut à droite) > Paramètres > Afficher les paramètres avancés > Effacer les données de navigation > Paramètres de contenu > Cookies et données de site. Supprimez la ligne contenant l’adresse de notre site internet. Pour plus d’information sur les cookies, leur utilité et leur gestion par l’internaute, vous pouvez vous reporter au site de la CNIL.
				</div>

				<div class="section-page-enfi section-mention">
					<p>7 - DONNÉES PERSONNELLES</p>
					La collecte de vos données personnelles est obligatoire pour le traitement de votre demande d'inscription à un événement. Vous êtes également susceptibles de recevoir des informations sur l’actualité de l’école de notre part pour des événements analogues à ceux que vous avez commandés. Si vous ne le souhaitez pas, adressez un mail à l'adresse <a href="mailto:bal@enfi.fr">bal@enfi.fr</a>Conformément à la loi n°78-18 du 6 janvier 1978 modifiée en 2004, vous disposez d'un droit d'accès, de rectification et de suppression pour les informations qui vous concernent, que vous pouvez exercer par courrier à l'adresse suivante : ENFI, 4 Quai de Bercy 94224 Charenton Cedex. Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.
				</div>

				 
				<div class="section-page-enfi section-mention">
					<p>8– COLLECTE D’INFORMATIONS NOMINATIVES</p>
					Les informations nominatives collectées sur le site sont exclusivement destinées à l’ENFI à titre non commercial. L’ENFI s'engage à ne pas divulguer, ni vendre, ni louer, ni échanger à des tiers ou à quelque organisation extérieure que ce soit en vue de leur utilisation sans autorisation les informations que vous nous fournissez (à savoir votre société, votre nom, votre adresse, votre téléphone et votre adresse électronique).
				</div>

				<div class="section-page-enfi section-mention">
					La collecte de vos données personnelles est obligatoire pour le traitement de votre demande d'inscription à un événement. Vous êtes également susceptibles de recevoir des informations sur l’actualité de l’école de notre part pour des événements analogues à ceux que vous avez commandés. Si vous ne le souhaitez pas, adressez un mail à l'adresse <a href="mailto:bal@enfi.fr">bal@enfi.fr</a> Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.
				</div>

				<div class="section-page-enfi section-mention">
					Conformément à la loi n°78-18 du 6 janvier 1978 modifiée en 2004, vous disposez d'un droit d'accès, de rectification et de suppression pour les informations qui vous concernent, que vous pouvez exercer par courrier à l'adresse suivante :
				</div>

				<div class="section-page-enfi section-mention-adresse">
					ENFI,
					4 Quai de Bercy
					94224 Charenton Cedex.
				</div>

				<div class="section-page-enfi section-mention">
					<p>9 - LOI APPLICABLE</p>
					Le contenu du site enfi.fr est assujetti au droit applicable en France.  Tout utilisateur reconnaît la compétence des tribunaux français pour tout ce qui concerne le contenu et l'utilisation du site ou les recours en découlant.
				</div>
      		</div>
		</div>
	</div>
</div>



<script>
    var v = document.getElementById('videoPlayer');

    v.play()
    v.volume = 0.5;

    v.addEventListener('click',function(){
        if (v.paused == false) {
            v.pause();
            v.firstChild.nodeValue = 'Play';
        }
        else {
            v.play();
            v.firstChild.nodeValue = 'Pause';
        }
    });
</script>





<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


<?php wp_footer(); ?>

</body>
</html>
