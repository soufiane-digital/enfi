<?php

/**
Template Name: catalogue-des-formations
**/

$args = array(); 

$taxo = get_query_var('type_formation', null);
if($taxo) {
	$args = array(
		'tax_query' => array(
			'relation' => 'AND',
			array(
    		'taxonomy'  =>  'type_formation',
    		'field'     =>  'slug',
    		'terms'   =>  $taxo,
			)
		)
	);
} 

$taxo = get_query_var('categorie_formation', null);
if($taxo) {
	$args = array(
		'tax_query' => array(
			'relation' => 'AND',
			array(
    		'taxonomy'  =>  'categorie_formation',
    		'field'     =>  'slug',
    		'terms'   =>  $taxo,
			)
		)
	);
} 
					
get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="headband" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/catalogue/bck-catalogue.jpg);" >

			<div class="container">
				<div class="row">
					<div class="col-md-4 mobile-headband">
						<div class="content-headband">
							<img src="<?php echo get_template_directory_uri(); ?>/img/catalogue/icon-catalogue.png" alt="">
							<h1>Le catalogue des formations</h1>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// SIDEBARE ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="row">

					<!-- SIDEBARE -->
					<form method="POST" id="form-filter">

						<div class="col-md-4 mobile-headband">
							<div class="sidebar-catalogue">
								<div class="bck-black">

									<div class="fil-ariane fil-white">
										<a href="">Accueil</a>
										<a href="">></a>
										<a class="active" href="">Formations</a>
									</div>
									<h2>thème de votre formation</h2>
									<div class="formation-catalogue">
										<div class="row">
											<?php $taxos = get_terms('categorie_formation');?>

											<ul class="ul-list-link">
												<?php 
													foreach ($taxos as $taxo):
														if(isset($_GET['categorie_formation']) && $_GET['categorie_formation'] === $taxo->slug) {$active = 'active';} else {$active = '';}
												?>

													<li class="col-md-12 col-sm-6" class="list-link">
														<a href="javascript:void(0);" data-slug="<?php echo $taxo->slug; ?>" class="link-taxo">
															<div class="bd-formation <?php echo $active; ?>">
																<div class="picto" style="background-color: <?php the_field('couleur', 'categorie_formation_'.$taxo->term_id); ?>">
																	<img src="<?php echo get_field('picto', 'categorie_formation_'.$taxo->term_id)['sizes']['thumbnail'] ?>" alt="">
																</div>
																<h4><?php echo $taxo->name; ?></h4>
															</div>
														</a>
													</li>
													
												<?php endforeach; ?>

											</ul>
										</div>
									</div>

									<div class="type-formation">
										<h2>TYPE de formation</h2>

										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="formation-distance-cat">
													<div class="row">
														<?php $types=get_terms('type_formation', array('parent' => 0));?>
														<?php foreach($types as $type):?>
															<div class="col-md-12 col-sm-6">
																<div class="formation-checkbox">
																	<h4><?php echo $type->name ?></h4>
																	<?php 
																	$names = get_terms('type_formation', array('child_of' => $type->term_id));
																	foreach($names as $name): 
			                            	if(isset($_GET['type_formation']) && $_GET['type_formation'] === $name->slug) {$active = true;} else {$active = false;}?>

																		<div class="checkbox">
																			<label class="label-formation">
																				<input type="checkbox" class="checkbox" name="type_formation" value="<?php echo $name->slug ?>" <?php echo $active ? 'checked' : ''; ?>> <?php echo $name->name;?>
																			</label>
																		</div>

																	<?php endforeach;?>
																</div>
															</div>
														<?php endforeach;?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<a href="javascript:void(0);" class="red-btn" id="formations-liste">réinitialiser les filtres</a>
								</div>
							</div>
						</div>
						<input type="hidden" name="categorie_formation" id="hidden">
						<input type="hidden" name="page" class="hidden-page">
					</form>

					<!-- BLOC DE FORMATION -->

					<div class="col-md-8">
						<div class="row">
							<div class="bloc-catalogue-formation" id="scroll-to-formation">
								<?php listing_training_posts(1, $args); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php

get_footer();