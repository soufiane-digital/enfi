<?php

/**
Template Name: actualite
**/

get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->

<div class="container-fluid">
	<div class="row">
		<div class="headband" style="background-image: url(<?php the_field('image'); ?>);" >

			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="content-headband page-list">
							<img src="<?php echo get_template_directory_uri(); ?>/img/catalogue/icon-catalogue.png" alt="">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="row">
					<div class="zone-actus hover-mask-bloc">
						<?php
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						query_posts( 'posts_per_page=9&paged=' . $paged ); 
						while ( have_posts() ) : the_post(); ?>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="bck-page-actu">
									<div class="video-right article-actu hover-mask">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<?php 
											$image = get_field('image_une');
											if( !empty($image) ): ?>
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											<?php endif; ?>
											<span class="glyphicon glyphicon-search"></span>
											<div class="titre-actu-p">
												<h2>
													<?php 
													$messagealimiter=get_the_title();
													echo substr($messagealimiter, 0,90);
													if(strlen ($messagealimiter) > 90){echo "...";}
													?>
												</h2>
											</div>
											<div class="date">
												<i class="fa fa-calendar-o"></i>
												<p><?php the_time('j/m/Y'); ?></p>
											</div>
											<div class="description-actu">
												<?php 
												$messagealimiter=get_field('extrait');
												echo substr($messagealimiter, 0,120);
												if(strlen ($messagealimiter) > 120){echo "...";}
												?>
												<div class="row">	
													<span class="icon-plus"></span>
												</div>
											</div>
											<div class="row">
												<div class="visible-xs">
													<div class="btn-actu-call">Lire l'article</div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						<div class="clearfix"></div>
					</div>
				</div>
				<div id="pagination"> 
					<?php echo paginate_links(); ?>
				</div>
				<?php wp_reset_query(); ?>
				<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php

get_footer();