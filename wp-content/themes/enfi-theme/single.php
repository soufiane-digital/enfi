<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package enfi-theme
 */

get_header(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- ////////////////// HEADBAND ///////////////////// -->
<!-- ///////////////////////////////////////////////// -->
<?php
$the_query_1 = new WP_Query( array( 'pagename' => 'actualites-de-lenfi' ) );
if ( $the_query_1->have_posts() ) : $the_query_1->the_post(); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="headband" style="background-image: url(<?php the_field('image'); ?>);" >
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="content-headband actualites">
								<span class="icon-catalogue"></span>
								<strong><?php the_title(); ?></strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php wp_reset_query(); ?>

<!-- ///////////////////////////////////////////////// -->
<!-- //////////////    ZOOM ACTU    ////////////////// -->
<!-- ///////////////////////////////////////////////// -->


<div class="container-fluid">
	<div class="row">
		<div class="bck-content">
			<div class="container">
				<div class="bck-page bck-top ">
					<div class="row">
						<div class="col-md-12">
							<!-- ///////////// TTRE ACTU ENFI ///////////// -->
							<div class="fil-ariane">
								<a href="<?php echo site_url(); ?>">Accueil ></a>
								<a href="<?php echo site_url(); ?>/?page_id=86">Actualités ></a>
								<a class="active" href=""><?php the_title(); ?></a>
							</div>
							<div class="titre-enfi">
								<h1><?php the_title(); ?></h1>
								<div class="date">
									<i class="fa fa-calendar-o"></i>
									<p><?php the_time('j/m/Y'); ?></p>
								</div>
							</div>
							<?php
							if( have_rows('contenus') ):
								while ( have_rows('contenus') ) : the_row(); ?>

									<?php if( get_row_layout() == 'h2' ): ?>
        								<div class="titre-enfi">
											<h2><?php the_sub_field('titre'); ?></h2>
										</div>

									<?php elseif( get_row_layout() == 'h3' ): ?>
	        							<div class="section-page-enfi">
											<h3><?php the_sub_field('titre'); ?></h3>
										</div>

									<?php elseif( get_row_layout() == 'h4' ): ?>
	        							<div class="section-page-enfi">
	        								<ul>
	        									<li>
													<h4><?php the_sub_field('titre'); ?></h4>
												</li>
											</ul>
										</div>

									<?php elseif( get_row_layout() == 'h5' ): ?>
	        							<div class="section-page-enfi container-h5">
											<h5><?php the_sub_field('titre'); ?></h5>
										</div>

									<?php elseif( get_row_layout() == 'chapeau' ): ?> 
										<div class="sous-titre-enfi">
											<?php the_sub_field('texte'); ?>
										</div>

									<?php elseif( get_row_layout() == 'paragraphe' ): ?> 
										<div class="section-page-enfi">
											<div class="txt-page-enfi">
												<?php the_sub_field('texte'); ?>
											</div>
										</div>

									<?php elseif( get_row_layout() == 'zone_doc' ): ?> 
										<div class="section-page-enfi">
											<div class="page-presse">
												<?php
												if( have_rows('documents') ):
													while ( have_rows('documents') ) : the_row(); ?>
														<a href="<?php the_sub_field('fichier_pdf'); ?>" target="_blank" title="<?php the_sub_field('titre'); ?>">
															<div class="section-download-presse">
																<i class="fa fa-download"></i>
																<p><?php the_sub_field('titre'); ?></p>
															</div>
														</a>
													<?php endwhile;
												endif; ?>
											</div>
										</div>

									<?php elseif( get_row_layout() == 'liste_logos' ): ?> 		
										<div class="section-page-enfi page-partenaires">
											<?php
											if( have_rows('logos') ):
												while ( have_rows('logos') ) : the_row(); ?>
													<a href="<?php the_sub_field('site_partenaire'); ?>" target="_blank" title="Aller sur le site du partenaire">
														<?php 
														$logo = get_sub_field('logo');
														if( !empty($logo) ): ?>
															<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
														<?php endif; ?>
													</a>
												<?php endwhile;
											endif; ?>
											<div class="clearfix"></div>
										</div>
										

									<?php elseif( get_row_layout() == 'liste-formations' ): ?>
										<div id="liste-formation-home">
											<div class="row">
												<div class="liste-formation-home">
													<ul>
														<?php
														if( have_rows('formations') ):
															while ( have_rows('formations') ) : the_row(); ?>
																<?php 
																$term = get_sub_field('formation');
																if( $term ): ?>
																	<li>
																		<a class="<?php echo get_field('class_css', $term); ?>" href="">
																			<?php 
																			$image = get_field('picto', $term);
																			if( !empty($image) ): ?>
																				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																			<?php endif; ?>
																			<h2><?php echo $term->name; ?></h2>
																		</a>
																	</li>
																<?php endif; ?>
															<?php 
															endwhile;
														endif; ?>
													</ul>
												</div>
											</div>
										</div>


									<?php elseif( get_row_layout() == 'zone_lien' ): ?>
										<div class="btn-inscription">
											<h3><?php the_sub_field('texte_incitatif'); ?></h3>
											<a class="red-btn" href="<?php the_sub_field('lien'); ?>" title="<?php the_sub_field('libelle'); ?>"><?php the_sub_field('libelle'); ?></a>
										</div>
										<div class="clearfix"></div>

									<?php elseif( get_row_layout() == 'paragraphe_image' ): ?> 
										<div class="page-content-enfi">
											<div class="row">
												<div class="col-md-8">
													<?php
													if( have_rows('contenus_gauche') ):
														while ( have_rows('contenus_gauche') ) : the_row(); ?>

															<?php if( get_row_layout() == 'h2' ): ?>
																<div class="titre-enfi">
																	<h2><?php the_sub_field('titre'); ?></h2>
																</div>

															<?php elseif( get_row_layout() == 'h3' ): ?>
																<div class="section-page-enfi">
																	<h3><?php the_sub_field('titre'); ?></h3>
																</div>

															<?php elseif( get_row_layout() == 'h4' ): ?>
																<div class="titre-enfi">
																	<h4><?php the_sub_field('titre'); ?></h4>
																</div>

															<?php elseif( get_row_layout() == 'chapeau' ): ?>
																<div class="sous-titre-enfi">
																	<?php the_sub_field('texte'); ?>
																</div>

															<?php elseif( get_row_layout() == 'paragraphe' ): ?>
																<div class="section-page-enfi">
																	<div class="txt-page-enfi">
																		<?php the_sub_field('texte'); ?>
																	</div>
																</div>
															<?php endif; ?>
														<?php endwhile; 
													endif; ?>
												</div>
												<div class="col-md-4">
													<div class="img-enfi">
														<?php 
														$image = get_sub_field('image');
														if( !empty($image) ): ?>
															<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
														<?php endif; ?>
													</div>
												</div>
											</div>
										<div class="clearfix"></div>
	
									<?php endif; ?>

								<?php endwhile;
							endif; ?>

							<!-- /////////////// REDACTEUR ZOOM /////////////// -->

								<?php 
								$auteur = get_field('nom_auteur');
								if( !empty($auteur) ): ?>
									<div class="redacteur-zoom">
										<?php 
										$photo = get_field('photo');
										if( !empty($photo) ): ?>
											<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
										<?php endif; ?>
										<div class="redacteur-nom"><?php the_field('nom_auteur'); ?> </div>
										<div class="redacteur-poste">- <?php the_field('fonction_auteur'); ?></div>
									</div>
								<?php endif; ?>

								<div class="partage-actu-single">
									<?php include('inc/partager-actu.php') ?>
								</div>

							</div>
						</div>
					</div>
				<div class="clearfix"></div>
				</div>

				<!-- /////////////// ACTU ZOOM /////////////// -->

				<div class="actu-zoom-p">
					<?php 
					$posts = get_field('suggestions');
					if( $posts ): ?>
						<div class="sug-form">
						<div class="container">
							
								<span class="titre-rubrique centrer"><?php the_field('titre_rubrique'); ?></span>
								<div class="row">
								<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
    							<?php setup_postdata($post); ?>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="bck-page-actu">
											<div class="video-right article-actu hover-mask">
												<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
													<?php 
													$image = get_field('image_une');
													if( !empty($image) ): ?>
														<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
													<?php endif; ?>
													<span class="glyphicon glyphicon-search"></span>
													<div class="titre-actu-p">
														<h2><?php the_title(); ?></h2>
													</div>
													<div class="date">
														<i class="fa fa-calendar-o"></i>
														<p><?php the_time('j/m/Y'); ?></p>
													</div>
													<div class="description-actu">
														<?php 
														$messagealimiter=get_field('extrait');
														echo substr($messagealimiter, 0,115);
														if(strlen ($messagealimiter) > 115){echo "...";}
														?>
														<span class="icon-enfi-plus-2"></span>
													</div>
												</a>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							
							<div class="col-md-12 col-sm-6 col-xs-12">
								<div class="btn-voir-actu">
									<a class="red-btn" href="<?php echo site_url(); ?>/?page_id=86">voir toutes les actualités</a>
								</div>
							</div>
						</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
get_footer();