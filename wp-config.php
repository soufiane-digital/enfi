<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'enfi');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'toor');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/!dE}h;.1%dRB8uv,buWW41TVh{Vj<o|4_37t>8qeuFu[f?i7b,)`-j+q4VU@|XG');
define('SECURE_AUTH_KEY',  'zdOLB p bd<8/:WmHKU[W+j=6OWfb|2W(7x)|!cd44DtJk7?=#6bOOc1uLJV0]A7');
define('LOGGED_IN_KEY',    'Jc^VNI9`tN8 oWP,--e@:X(pXd:X0t+).5K+:d mT^ %hfN=?1T9|WGDW^=7A}9l');
define('NONCE_KEY',        '|<_MA2$|@tdiz2mMY2$K?H-1D|g32t&3F(Zr3e==I@Xv(1,8fLc~87HHZlhcZkgC');
define('AUTH_SALT',        'BH-s(Ccq.} >v5znjw:o.LE02BQtj^Gea.S7*-|Y K?<!_:z|wH#h?.cf;c^Fk[F');
define('SECURE_AUTH_SALT', 'W{O2#y^[Qoc3dR(3>I=7;leL`@b)f(RXx4!8zw}yjQs3e+$J&irHR(p([Jtw<Q-i');
define('LOGGED_IN_SALT',   'J--w#O3b&1LBti+YRqxlK=fY=1l5~6&4p W=~Qa_1xB`D6%v.)x-e|9#-;Z%}c7l');
define('NONCE_SALT',       'f,1S6@8zl[AxGhI*>:WMF?N lU&0AD95SE9oDsCm7#<w|+F@IQ3_-HzyEL5qfv#Y');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', true);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');